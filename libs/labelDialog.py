try:
    from PyQt5.QtGui import *
    from PyQt5.QtCore import *
    from PyQt5.QtWidgets import *
except ImportError:
    from PyQt4.QtGui import *
    from PyQt4.QtCore import *

from libs.lib import newIcon, labelValidator

BB = QDialogButtonBox


class LabelDialog(QDialog):

    def __init__(self, text="Enter object label", parent=None, listItem=None):
        super(LabelDialog, self).__init__(parent)

        self.onlyInt = QIntValidator(0,10)
        self.edit = QLineEdit()
        if listItem is not None and len(listItem) > 0 and text !='Enter object label':
            self.edit.setText(str(listItem.index(text)))
        self.edit.setValidator(labelValidator())
        #self.edit.editingFinished.connect(self.postProcess)
        self.edit.setValidator(self.onlyInt)
        self.edit.textEdited.connect(self.postProcess)

        model = QStringListModel()
        model.setStringList(listItem)
        completer = QCompleter()
        completer.setModel(model)
        self.edit.setCompleter(completer)

        layout = QVBoxLayout()
        layout.addWidget(self.edit)
        #self.buttonBox = bb = BB(BB.Ok | BB.Cancel, Qt.Horizontal, self)
        #bb.button(BB.Ok).setIcon(newIcon('done'))
        #bb.button(BB.Cancel).setIcon(newIcon('undo'))
        #bb.accepted.connect(self.validate)
        #bb.rejected.connect(self.reject)
        #layout.addWidget(bb)

        if listItem is not None and len(listItem) > 0:
            self.listWidget = QListWidget(self)
            for item in listItem:
                self.listWidget.addItem(str(listItem.index(item)) + ' ' + item)
            self.listWidget.itemClicked.connect(self.listItemClick)
            self.listWidget.itemDoubleClicked.connect(self.listItemDoubleClick)
            layout.addWidget(self.listWidget)

        self.setLayout(layout)

    def validate(self):
        try:
            if self.edit.text().trimmed():
                self.accept()
        except AttributeError:
            # PyQt5: AttributeError: 'str' object has no attribute 'trimmed'
            if self.edit.text().strip():
                self.accept()

    def postProcess(self):
        if self.edit.text().strip() == '':
            return; 
        try:
            text =self.listWidget.item(int(self.edit.text())).text()
            self.edit.setText(text[text.find(' ') +1:])
            self.validate()
        except:
            # PyQt5: AttributeError: 'str' object has no attribute 'trimmed'
            self.edit.setText('')        

    def popUp(self, text='', move=True):
        self.edit.setText(text)
        self.edit.setSelection(0, len(text))
        self.edit.setFocus(Qt.PopupFocusReason)
        if move:
            self.move(QCursor.pos())
        return self.edit.text() if self.exec_() else None

    def listItemClick(self, tQListWidgetItem):
        try:
            text = tQListWidgetItem.text()[2:]
            text = text[text.find(' ')+1:]
        except AttributeError:
            # PyQt5: AttributeError: 'str' object has no attribute 'trimmed'
            text = tQListWidgetItem.text().strip()
        self.edit.setText(text)
        
    def listItemDoubleClick(self, tQListWidgetItem):
        self.listItemClick(tQListWidgetItem)
        self.validate()
