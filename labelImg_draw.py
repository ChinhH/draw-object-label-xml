#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
from lxml import etree
from math import sqrt
#from libs.ustr import ustr
import hashlib
import codecs
import distutils.spawn
import os.path
import platform
import re
import sys
import subprocess
import cv2
#from tkinter import filedialog
#from PIL import Image as imageofpil
from functools import partial
from collections import defaultdict
import pickle
import os
import io
import httplib2
try:
    from PyQt5.QtGui import *
    from PyQt5.QtCore import *
    from PyQt5.QtWidgets import *
except ImportError:
    # needed for py3+qt4
    # Ref:
    # http://pyqt.sourceforge.net/Docs/PyQt4/incompatible_apis.html
    # http://stackoverflow.com/questions/21217399/pyqt4-qtcore-qvariant-object-instead-of-a-string
    if sys.version_info.major >= 3:
        import sip
        sip.setapi('QVariant', 2)
    from PyQt4.QtGui import *
    from PyQt4.QtCore import *

import resources
# Add internal libs
#from constants import *
#from lib import struct, newAction, newIcon, addActions, fmtShortcut, generateColorByText
#from settings import Settings
#from shape import Shape, DEFAULT_LINE_COLOR, DEFAULT_FILL_COLOR
#from canvas import Canvas
#from zoomWidget import ZoomWidget
#from labelDialog import LabelDialog
#from colorDialog import ColorDialog
#from labelFile import LabelFile, LabelFileError
#from toolBar import ToolBar
#from pascal_voc_io import PascalVocReader
#from pascal_voc_io import XML_EXT
#from yolo_io import YoloReader
#from yolo_io import TXT_EXT
#from ustr import ustr
#from version import __version__
#import getpass
#import uuid
#from wand.image import Image
from wand.image import Image
from PIL import Image as Img
import numpy as np
print('Please wait...')
print('Need more GPU to faster...')

#from apiclient import discovery
#from oauth2client import client
#from oauth2client import tools
#from oauth2client.file import Storage
#from apiclient.http import MediaFileUpload, MediaIoBaseDownload

#try:
#    import argparse
#    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
#except ImportError:
#    flags = None

#import tensorflow as tf
#from tensorflow.contrib import learn
print('Thanks for your times!')
kernel5 = np.ones((5,5),np.uint8)
xml = ""
giatridau = 0
allname = ""
imagenow = ""
username = ""#getpass.getuser()
             #Path_image =
                          #'D:/Project-test-data/KENSHIN/Show_OCR_Kenshin/Image'
                          #folder = Path_image + "/" + username
folder = ''
foldermodel = ''
__appname__ = 'VBPO_CROP_IMAGE_AI'

#FLAGS = tf.app.flags.FLAGS

#tf.app.flags.DEFINE_string('model','',
#                          """Directory for model checkpoints""")
#tf.app.flags.DEFINE_string('device','/cpu:0',
#                        """Device for graph placement""")

#tf.logging.set_verbosity(tf.logging.WARN)



# Non-configurable parameters
#mode = learn.ModeKeys.INFER # 'Configure' training mode for dropout layers
##region tensorflow
#layer_params = [[64, 3, 'valid', 'conv1', False], 
#                 [64, 3, 'same',  'conv2', True], # pool
#                 [128, 3, 'same',  'conv3', False], 
#                 [128, 3, 'same',  'conv4', True], # hpool
#                 [256, 3, 'same',  'conv5', False],
#                 [256, 3, 'same',  'conv6', True], # hpool
#                 [512, 3, 'same',  'conv7', False], 
#                 [512, 3, 'same',  'conv8', True]] # hpool 3
#rnn_size = 2 ** 9
#dropout_rate = 0.5



#SCOPES = 'https://www.googleapis.com/auth/drive'
#CLIENT_SECRET_FILE = 'credentials.json'
#APPLICATION_NAME = 'Drive API Python Quickstart'

#os.environ["GOOGLE_APPLICATION_CREDENTIALS"]=".\ocr vbpo-4ef67c3bf53d.json"
XML_EXT = '.xml'
ENCODE_METHOD = 'utf-8'
class LabelFileError(Exception):
    pass


class LabelFile(object):
    # It might be changed as window creates. By default, using XML ext
    # suffix = '.lif'
    suffix = XML_EXT

    def __init__(self, filename=None):
        self.shapes = ()
        self.imagePath = None
        self.imageData = None
        self.verified = False

    def savePascalVocFormat(self, filename, shapes, imagePath, imageData,
                            lineColor=None, fillColor=None, databaseSrc=None):
        imgFolderPath = os.path.dirname(imagePath)
        imgFolderName = os.path.split(imgFolderPath)[-1]
        imgFileName = os.path.basename(imagePath)
        #imgFileNameWithoutExt = os.path.splitext(imgFileName)[0]
        # Read from file path because self.imageData might be empty if saving to
        # Pascal format
        image = QImage()
        image.load(imagePath)
        imageShape = [image.height(), image.width(),
                      1 if image.isGrayscale() else 3]
        writer = PascalVocWriter(imgFolderName, imgFileName,
                                 imageShape, localImgPath=imagePath)
        writer.verified = self.verified

        for shape in shapes:
            points = shape['points']
            label = shape['label']
            # Add Chris
            difficult = int(shape['difficult'])
            bndbox = LabelFile.convertPoints2BndBox(points)
            writer.addBndBox(bndbox[0], bndbox[1], bndbox[2], bndbox[3], label, difficult)

        writer.save(targetFile=filename)
        return

    def saveYoloFormat(self, filename, shapes, imagePath, imageData, classList,
                            lineColor=None, fillColor=None, databaseSrc=None):
        imgFolderPath = os.path.dirname(imagePath)
        imgFolderName = os.path.split(imgFolderPath)[-1]
        imgFileName = os.path.basename(imagePath)
        #imgFileNameWithoutExt = os.path.splitext(imgFileName)[0]
        # Read from file path because self.imageData might be empty if saving to
        # Pascal format
        image = QImage()
        image.load(imagePath)
        imageShape = [image.height(), image.width(),
                      1 if image.isGrayscale() else 3]
        writer = YOLOWriter(imgFolderName, imgFileName,
                                 imageShape, localImgPath=imagePath)
        writer.verified = self.verified

        for shape in shapes:
            points = shape['points']
            label = shape['label']
            # Add Chris
            difficult = int(shape['difficult'])
            bndbox = LabelFile.convertPoints2BndBox(points)
            writer.addBndBox(bndbox[0], bndbox[1], bndbox[2], bndbox[3], label, difficult)

        writer.save(targetFile=filename, classList=classList)
        return

    def toggleVerify(self):
        self.verified = not self.verified

    ''' ttf is disable
    def load(self, filename):
        import json
        with open(filename, 'rb') as f:
                data = json.load(f)
                imagePath = data['imagePath']
                imageData = b64decode(data['imageData'])
                lineColor = data['lineColor']
                fillColor = data['fillColor']
                shapes = ((s['label'], s['points'], s['line_color'], s['fill_color'])\
                        for s in data['shapes'])
                # Only replace data after everything is loaded.
                self.shapes = shapes
                self.imagePath = imagePath
                self.imageData = imageData
                self.lineColor = lineColor
                self.fillColor = fillColor

    def save(self, filename, shapes, imagePath, imageData, lineColor=None, fillColor=None):
        import json
        with open(filename, 'wb') as f:
                json.dump(dict(
                    shapes=shapes,
                    lineColor=lineColor, fillColor=fillColor,
                    imagePath=imagePath,
                    imageData=b64encode(imageData)),
                    f, ensure_ascii=True, indent=2)
    '''

    @staticmethod
    def isLabelFile(filename):
        fileSuffix = os.path.splitext(filename)[1].lower()
        return fileSuffix == LabelFile.suffix

    @staticmethod
    def convertPoints2BndBox(points):
        xmin = float('inf')
        ymin = float('inf')
        xmax = float('-inf')
        ymax = float('-inf')
        for p in points:
            x = p[0]
            y = p[1]
            xmin = min(x, xmin)
            ymin = min(y, ymin)
            xmax = max(x, xmax)
            ymax = max(y, ymax)

        # Martin Kersner, 2015/11/12
        # 0-valued coordinates of BB caused an error while
        # training faster-rcnn object detector.
        if xmin < 1:
            xmin = 1

        if ymin < 1:
            ymin = 1

        return (int(xmin), int(ymin), int(xmax), int(ymax))
__version_info__ = ('1', '7', '0')
__version__ = '.'.join(__version_info__)
TXT_EXT = '.txt'
ENCODE_METHOD = 'utf-8'

class YOLOWriter:

    def __init__(self, foldername, filename, imgSize, databaseSrc='Unknown', localImgPath=None):
        self.foldername = foldername
        self.filename = filename
        self.databaseSrc = databaseSrc
        self.imgSize = imgSize
        self.boxlist = []
        self.localImgPath = localImgPath
        self.verified = False

    def addBndBox(self, xmin, ymin, xmax, ymax, name, difficult):
        bndbox = {'xmin': xmin, 'ymin': ymin, 'xmax': xmax, 'ymax': ymax}
        bndbox['name'] = name
        bndbox['difficult'] = difficult
        self.boxlist.append(bndbox)

    def BndBox2YoloLine(self, box, classList=[]):
        xmin = box['xmin']
        xmax = box['xmax']
        ymin = box['ymin']
        ymax = box['ymax']

        xcen = float((xmin + xmax)) / 2 / self.imgSize[1]
        ycen = float((ymin + ymax)) / 2 / self.imgSize[0]

        w = float((xmax - xmin)) / self.imgSize[1]
        h = float((ymax - ymin)) / self.imgSize[0]

        classIndex = classList.index(box['name'])

        return classIndex, xcen, ycen, w, h

    def save(self, classList=[], targetFile=None):

        out_file = None #Update yolo .txt
        out_class_file = None   #Update class list .txt

        if targetFile is None:
            out_file = open(
            self.filename + TXT_EXT, 'w', encoding=ENCODE_METHOD)
            classesFile = os.path.join(os.path.dirname(os.path.abspath(self.filename)), "classes.txt")
            out_class_file = open(classesFile, 'w')

        else:
            out_file = codecs.open(targetFile, 'w', encoding=ENCODE_METHOD)
            classesFile = os.path.join(os.path.dirname(os.path.abspath(targetFile)), "classes.txt")
            out_class_file = open(classesFile, 'w')


        for box in self.boxlist:
            classIndex, xcen, ycen, w, h = self.BndBox2YoloLine(box, classList)
            print (classIndex, xcen, ycen, w, h)
            out_file.write("%d %.6f %.6f %.6f %.6f\n" % (classIndex, xcen, ycen, w, h))

        print (classList)
        print (out_class_file)
        for c in classList:
            out_class_file.write(c+'\n')

        out_class_file.close()
        out_file.close()



class YoloReader:

    def __init__(self, filepath, image, classListPath=None):
        # shapes type:
        # [labbel, [(x1,y1), (x2,y2), (x3,y3), (x4,y4)], color, color, difficult]
        self.shapes = []
        self.filepath = filepath

        if classListPath is None:
            dir_path = os.path.dirname(os.path.realpath(self.filepath))
            self.classListPath = os.path.join(dir_path, "classes.txt")
        else:
            self.classListPath = classListPath

        print (filepath, self.classListPath)

        classesFile = open(self.classListPath, 'r')
        self.classes = classesFile.read().strip('\n').split('\n')

        print (self.classes)

        imgSize = [image.height(), image.width(),
                      1 if image.isGrayscale() else 3]

        self.imgSize = imgSize

        self.verified = False
        # try:
        self.parseYoloFormat()
        # except:
            # pass

    def getShapes(self):
        return self.shapes

    def addShape(self, label, xmin, ymin, xmax, ymax, difficult):

        points = [(xmin, ymin), (xmax, ymin), (xmax, ymax), (xmin, ymax)]
        self.shapes.append((label, points, None, None, difficult))

    def yoloLine2Shape(self, classIndex, xcen, ycen, w, h):
        label = self.classes[int(classIndex)]

        xmin = max(float(xcen) - float(w) / 2, 0)
        xmax = min(float(xcen) + float(w) / 2, 1)
        ymin = max(float(ycen) - float(h) / 2, 0)
        ymax = min(float(ycen) + float(h) / 2, 1)

        xmin = int(self.imgSize[1] * xmin)
        xmax = int(self.imgSize[1] * xmax)
        ymin = int(self.imgSize[0] * ymin)
        ymax = int(self.imgSize[0] * ymax)

        return label, xmin, ymin, xmax, ymax

    def parseYoloFormat(self):
        bndBoxFile = open(self.filepath, 'r')
        for bndBox in bndBoxFile:
            classIndex, xcen, ycen, w, h = bndBox.split(' ')
            label, xmin, ymin, xmax, ymax = self.yoloLine2Shape(classIndex, xcen, ycen, w, h)

            # Caveat: difficult flag is discarded when saved as yolo format.
            self.addShape(label, xmin, ymin, xmax, ymax, False)


class PascalVocWriter:

    def __init__(self, foldername, filename, imgSize,databaseSrc='Unknown', localImgPath=None):
        self.foldername = foldername
        self.filename = filename
        self.databaseSrc = databaseSrc
        self.imgSize = imgSize
        self.boxlist = []
        self.localImgPath = localImgPath
        self.verified = False

    def prettify(self, elem):
        """
            Return a pretty-printed XML string for the Element.
        """
        rough_string = ElementTree.tostring(elem, 'utf8')
        root = etree.fromstring(rough_string)
        return etree.tostring(root, pretty_print=True, encoding=ENCODE_METHOD).replace("  ".encode(), "\t".encode())
        # minidom does not support UTF-8
        '''reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="\t", encoding=ENCODE_METHOD)'''

    def genXML(self):
        """
            Return XML root
        """
        # Check conditions
        if self.filename is None or \
                self.foldername is None or \
                self.imgSize is None:
            return None

        top = Element('annotation')
        if self.verified:
            top.set('verified', 'yes')

        folder = SubElement(top, 'folder')
        folder.text = self.foldername

        filename = SubElement(top, 'filename')
        filename.text = self.filename

        if self.localImgPath is not None:
            localImgPath = SubElement(top, 'path')
            localImgPath.text = self.localImgPath

        source = SubElement(top, 'source')
        database = SubElement(source, 'database')
        database.text = self.databaseSrc

        size_part = SubElement(top, 'size')
        width = SubElement(size_part, 'width')
        height = SubElement(size_part, 'height')
        depth = SubElement(size_part, 'depth')
        width.text = str(self.imgSize[1])
        height.text = str(self.imgSize[0])
        if len(self.imgSize) == 3:
            depth.text = str(self.imgSize[2])
        else:
            depth.text = '1'

        segmented = SubElement(top, 'segmented')
        segmented.text = '0'
        return top

    def addBndBox(self, xmin, ymin, xmax, ymax, name, difficult):
        bndbox = {'xmin': xmin, 'ymin': ymin, 'xmax': xmax, 'ymax': ymax}
        bndbox['name'] = name
        bndbox['difficult'] = difficult
        self.boxlist.append(bndbox)

    def appendObjects(self, top):
        for each_object in self.boxlist:
            object_item = SubElement(top, 'object')
            name = SubElement(object_item, 'name')
            try:
                name.text = unicode(each_object['name'])
            except NameError:
                # Py3: NameError: name 'unicode' is not defined
                name.text = each_object['name']
            pose = SubElement(object_item, 'pose')
            pose.text = "Unspecified"
            truncated = SubElement(object_item, 'truncated')
            if int(each_object['ymax']) == int(self.imgSize[0]) or (int(each_object['ymin'])== 1):
                truncated.text = "1" # max == height or min
            elif (int(each_object['xmax'])==int(self.imgSize[1])) or (int(each_object['xmin'])== 1):
                truncated.text = "1" # max == width or min
            else:
                truncated.text = "0"
            difficult = SubElement(object_item, 'difficult')
            difficult.text = str( bool(each_object['difficult']) & 1 )
            bndbox = SubElement(object_item, 'bndbox')
            xmin = SubElement(bndbox, 'xmin')
            xmin.text = str(each_object['xmin'])
            ymin = SubElement(bndbox, 'ymin')
            ymin.text = str(each_object['ymin'])
            xmax = SubElement(bndbox, 'xmax')
            xmax.text = str(each_object['xmax'])
            ymax = SubElement(bndbox, 'ymax')
            ymax.text = str(each_object['ymax'])

    def save(self, targetFile=None):
        root = self.genXML()
        self.appendObjects(root)
        out_file = None
        if targetFile is None:
            out_file = codecs.open(
                self.filename + XML_EXT, 'w', encoding=ENCODE_METHOD)
        else:
            out_file = codecs.open(targetFile, 'w', encoding=ENCODE_METHOD)

        prettifyResult = self.prettify(root)
        out_file.write(prettifyResult.decode('utf8'))
        out_file.close()


class PascalVocReader:

    def __init__(self, filepath):
        # shapes type:
        # [labbel, [(x1,y1), (x2,y2), (x3,y3), (x4,y4)], color, color, difficult]
        self.shapes = []
        self.filepath = filepath
        self.verified = False
        try:
            self.parseXML()
        except:
            pass

    def getShapes(self):
        return self.shapes

    def addShape(self, label, bndbox, difficult):
        xmin = float(bndbox.find('xmin').text)
        ymin = float(bndbox.find('ymin').text)
        xmax = float(bndbox.find('xmax').text)
        ymax = float(bndbox.find('ymax').text)
        points = [(xmin, ymin), (xmax, ymin), (xmax, ymax), (xmin, ymax)]
        self.shapes.append((label, points, None, None, difficult))

    def parseXML(self):
        assert self.filepath.endswith(XML_EXT), "Unsupport file format"
        parser = etree.XMLParser(encoding=ENCODE_METHOD)
        xmltree = ElementTree.parse(self.filepath, parser=parser).getroot()
        filename = xmltree.find('filename').text
        try:
            verified = xmltree.attrib['verified']
            if verified == 'yes':
                self.verified = True
        except KeyError:
            self.verified = False

        for object_iter in xmltree.findall('object'):
            bndbox = object_iter.find("bndbox")
            label = object_iter.find('name').text
            # Add chris
            difficult = False
            if object_iter.find('difficult') is not None:
                difficult = bool(int(object_iter.find('difficult').text))
            self.addShape(label, bndbox, difficult)
        return True
SETTING_FILENAME = 'filename'
SETTING_RECENT_FILES = 'recentFiles'
SETTING_WIN_SIZE = 'window/size'
SETTING_WIN_POSE = 'window/position'
SETTING_WIN_GEOMETRY = 'window/geometry'
SETTING_LINE_COLOR = 'line/color'
SETTING_FILL_COLOR = 'fill/color'
SETTING_ADVANCE_MODE = 'advanced'
SETTING_WIN_STATE = 'window/state'
SETTING_SAVE_DIR = 'savedir'
SETTING_PAINT_LABEL = 'paintlabel'
SETTING_LAST_OPEN_DIR = 'lastOpenDir'
SETTING_AUTO_SAVE = 'autosave'
SETTING_SINGLE_CLASS = 'singleclass'
FORMAT_PASCALVOC='PascalVOC'
FORMAT_YOLO='YOLO'
DEFAULT_LINE_COLOR = QColor(0, 255, 0, 128)
DEFAULT_FILL_COLOR = QColor(255, 0, 0, 128)
DEFAULT_SELECT_LINE_COLOR = QColor(255, 255, 255)
DEFAULT_SELECT_FILL_COLOR = QColor(0, 128, 255, 155)
DEFAULT_VERTEX_FILL_COLOR = QColor(0, 255, 0, 255)
DEFAULT_HVERTEX_FILL_COLOR = QColor(255, 0, 0)
class Shape(object):
    P_SQUARE, P_ROUND = range(2)

    MOVE_VERTEX, NEAR_VERTEX = range(2)

    # The following class variables influence the drawing
    # of _all_ shape objects.
    line_color = DEFAULT_LINE_COLOR
    fill_color = DEFAULT_FILL_COLOR
    select_line_color = DEFAULT_SELECT_LINE_COLOR
    select_fill_color = DEFAULT_SELECT_FILL_COLOR
    vertex_fill_color = DEFAULT_VERTEX_FILL_COLOR
    hvertex_fill_color = DEFAULT_HVERTEX_FILL_COLOR
    point_type = P_ROUND
    point_size = 8
    scale = 1.0

    def __init__(self, label=None, line_color=None, difficult=False, paintLabel=False):
        self.label = label
        self.points = []
        self.fill = False
        self.selected = False
        self.difficult = difficult
        self.paintLabel = paintLabel

        self._highlightIndex = None
        self._highlightMode = self.NEAR_VERTEX
        self._highlightSettings = {
            self.NEAR_VERTEX: (4, self.P_ROUND),
            self.MOVE_VERTEX: (1.5, self.P_SQUARE),
        }

        self._closed = False

        if line_color is not None:
            # Override the class line_color attribute
            # with an object attribute. Currently this
            # is used for drawing the pending line a different color.
            self.line_color = line_color

    def close(self):
        self._closed = True

    def reachMaxPoints(self):
        if len(self.points) >= 4:
            return True
        return False

    def addPoint(self, point):
        if not self.reachMaxPoints():
            self.points.append(point)

    def popPoint(self):
        if self.points:
            return self.points.pop()
        return None

    def isClosed(self):
        return self._closed

    def setOpen(self):
        self._closed = False

    def paint(self, painter):
        if self.points:
            color = self.select_line_color if self.selected else self.line_color
            pen = QPen(color)
            # Try using integer sizes for smoother drawing(?)
            pen.setWidth(max(1, int(round(2.0 / self.scale))))
            painter.setPen(pen)

            line_path = QPainterPath()
            vrtx_path = QPainterPath()

            line_path.moveTo(self.points[0])
            # Uncommenting the following line will draw 2 paths
            # for the 1st vertex, and make it non-filled, which
            # may be desirable.
            #self.drawVertex(vrtx_path, 0)

            for i, p in enumerate(self.points):
                line_path.lineTo(p)
                self.drawVertex(vrtx_path, i)
            if self.isClosed():
                line_path.lineTo(self.points[0])

            painter.drawPath(line_path)
            painter.drawPath(vrtx_path)
            painter.fillPath(vrtx_path, self.vertex_fill_color)

            # Draw text at the top-left
            if self.paintLabel:
                min_x = sys.maxsize
                min_y = sys.maxsize
                for point in self.points:
                    min_x = min(min_x, point.x())
                    min_y = min(min_y, point.y())
                if min_x != sys.maxsize and min_y != sys.maxsize:
                    font = QFont()
                    font.setPointSize(16)
                    font.setBold(True)
                    painter.setFont(font)
                    if(self.label == None):
                        self.label = ""
                    painter.drawText(min_x, min_y, self.label)

            if self.fill:
                color = self.select_fill_color if self.selected else self.fill_color
                painter.fillPath(line_path, color)

    def drawVertex(self, path, i):
        d = self.point_size / self.scale
        shape = self.point_type
        point = self.points[i]
        if i == self._highlightIndex:
            size, shape = self._highlightSettings[self._highlightMode]
            d *= size
        if self._highlightIndex is not None:
            self.vertex_fill_color = self.hvertex_fill_color
        else:
            self.vertex_fill_color = Shape.vertex_fill_color
        if shape == self.P_SQUARE:
            path.addRect(point.x() - d / 2, point.y() - d / 2, d, d)
        elif shape == self.P_ROUND:
            path.addEllipse(point, d / 2.0, d / 2.0)
        else:
            assert False, "unsupported vertex shape"

    def nearestVertex(self, point, epsilon):
        for i, p in enumerate(self.points):
            if distance(p - point) <= epsilon:
                return i
        return None

    def containsPoint(self, point):
        return self.makePath().contains(point)

    def makePath(self):
        path = QPainterPath(self.points[0])
        for p in self.points[1:]:
            path.lineTo(p)
        return path

    def boundingRect(self):
        return self.makePath().boundingRect()

    def moveBy(self, offset):
        self.points = [p + offset for p in self.points]

    def moveVertexBy(self, i, offset):
        self.points[i] = self.points[i] + offset

    def highlightVertex(self, i, action):
        self._highlightIndex = i
        self._highlightMode = action

    def highlightClear(self):
        self._highlightIndex = None

    def copy(self):
        shape = Shape("%s" % self.label)
        shape.points = [p for p in self.points]
        shape.fill = self.fill
        shape.selected = self.selected
        shape._closed = self._closed
        if self.line_color != Shape.line_color:
            shape.line_color = self.line_color
        if self.fill_color != Shape.fill_color:
            shape.fill_color = self.fill_color
        shape.difficult = self.difficult
        return shape

    def __len__(self):
        return len(self.points)

    def __getitem__(self, key):
        return self.points[key]

    def __setitem__(self, key, value):
        self.points[key] = value

class ZoomWidget(QSpinBox):

    def __init__(self, value=100):
        super(ZoomWidget, self).__init__()
        self.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.setRange(1, 500)
        self.setSuffix(' %')
        self.setValue(value)
        self.setToolTip(u'Zoom Level')
        self.setStatusTip(self.toolTip())
        self.setAlignment(Qt.AlignCenter)

    def minimumSizeHint(self):
        height = super(ZoomWidget, self).minimumSizeHint().height()
        fm = QFontMetrics(self.font())
        width = fm.width(str(self.maximum()))
        return QSize(width, height)
class ToolBar(QToolBar):

    def __init__(self, title):
        super(ToolBar, self).__init__(title)
        layout = self.layout()
        m = (0, 0, 0, 0)
        layout.setSpacing(0)
        layout.setContentsMargins(*m)
        self.setContentsMargins(*m)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)

    def addAction(self, action):
        if isinstance(action, QWidgetAction):
            return super(ToolBar, self).addAction(action)
        btn = ToolButton()
        btn.setDefaultAction(action)
        btn.setToolButtonStyle(self.toolButtonStyle())
        self.addWidget(btn)


class ToolButton(QToolButton):
    """ToolBar companion class which ensures all buttons have the same size."""
    minSize = (60, 60)

    def minimumSizeHint(self):
        ms = super(ToolButton, self).minimumSizeHint()
        w1, h1 = ms.width(), ms.height()
        w2, h2 = self.minSize
        ToolButton.minSize = max(w1, w2), max(h1, h2)
        return QSize(*ToolButton.minSize)
BB = QDialogButtonBox


class ColorDialog(QColorDialog):

    def __init__(self, parent=None):
        super(ColorDialog, self).__init__(parent)
        self.setOption(QColorDialog.ShowAlphaChannel)
        # The Mac native dialog does not support our restore button.
        self.setOption(QColorDialog.DontUseNativeDialog)
        # Add a restore defaults button.
        # The default is set at invocation time, so that it
        # works across dialogs for different elements.
        self.default = None
        self.bb = self.layout().itemAt(1).widget()
        self.bb.addButton(BB.RestoreDefaults)
        self.bb.clicked.connect(self.checkRestore)

    def getColor(self, value=None, title=None, default=None):
        self.default = default
        if title:
            self.setWindowTitle(title)
        if value:
            self.setCurrentColor(value)
        return self.currentColor() if self.exec_() else None

    def checkRestore(self, button):
        if self.bb.buttonRole(button) & BB.ResetRole and self.default:
            self.setCurrentColor(self.default)



class LabelDialog(QDialog):

    def __init__(self, text="Enter object label", parent=None, listItem=None):
        super(LabelDialog, self).__init__(parent)

        self.onlyInt = QIntValidator(0,10)
        self.edit = QLineEdit()
        if listItem is not None and len(listItem) > 0 and text !='Enter object label':
            self.edit.setText(str(listItem.index(text)))
        self.edit.setValidator(labelValidator())
        #self.edit.editingFinished.connect(self.postProcess)
        self.edit.setValidator(self.onlyInt)
        self.edit.textEdited.connect(self.postProcess)

        model = QStringListModel()
        model.setStringList(listItem)
        completer = QCompleter()
        completer.setModel(model)
        self.edit.setCompleter(completer)

        layout = QVBoxLayout()
        layout.addWidget(self.edit)
        #self.buttonBox = bb = BB(BB.Ok | BB.Cancel, Qt.Horizontal, self)
        #bb.button(BB.Ok).setIcon(newIcon('done'))
        #bb.button(BB.Cancel).setIcon(newIcon('undo'))
        #bb.accepted.connect(self.validate)
        #bb.rejected.connect(self.reject)
        #layout.addWidget(bb)

        if listItem is not None and len(listItem) > 0:
            self.listWidget = QListWidget(self)
            for item in listItem:
                self.listWidget.addItem(str(listItem.index(item)) + ' ' + item)
            self.listWidget.itemClicked.connect(self.listItemClick)
            self.listWidget.itemDoubleClicked.connect(self.listItemDoubleClick)
            layout.addWidget(self.listWidget)

        self.setLayout(layout)

    def validate(self):
        try:
            if self.edit.text().trimmed():
                self.accept()
        except AttributeError:
            # PyQt5: AttributeError: 'str' object has no attribute 'trimmed'
            if self.edit.text().strip():
                self.accept()

    def postProcess(self):
        if self.edit.text().strip() == '':
            return; 
        try:
            text =self.listWidget.item(int(self.edit.text())).text()
            self.edit.setText(text[text.find(' ') +1:])
            self.validate()
        except:
            # PyQt5: AttributeError: 'str' object has no attribute 'trimmed'
            self.edit.setText('')        

    def popUp(self, text='', move=True):
        self.edit.setText(text)
        self.edit.setSelection(0, len(text))
        self.edit.setFocus(Qt.PopupFocusReason)
        if move:
            self.move(QCursor.pos())
        return self.edit.text() if self.exec_() else None

    def listItemClick(self, tQListWidgetItem):
        try:
            text = tQListWidgetItem.text()[2:]
            text = text[text.find(' ')+1:]
        except AttributeError:
            # PyQt5: AttributeError: 'str' object has no attribute 'trimmed'
            text = tQListWidgetItem.text().strip()
        self.edit.setText(text)
        
    def listItemDoubleClick(self, tQListWidgetItem):
        self.listItemClick(tQListWidgetItem)
        self.validate()
def ustr(x):
    '''py2/py3 unicode helper'''

    if sys.version_info < (3, 0, 0):
        from PyQt4.QtCore import QString
        if type(x) == str:
            return x.decode('utf-8')
        if type(x) == QString:
            return unicode(x)
        return x
    else:
        return x  # py3
def newIcon(icon):
    return QIcon('./icons/' + icon + '.png')


def newButton(text, icon=None, slot=None):
    b = QPushButton(text)
    if icon is not None:
        b.setIcon(newIcon(icon))
    if slot is not None:
        b.clicked.connect(slot)
    return b


def newAction(parent, text, slot=None, shortcut=None, icon=None,
              tip=None, checkable=False, enabled=True):
    """Create a new action and assign callbacks, shortcuts, etc."""
    a = QAction(text, parent)
    if icon is not None:
        a.setIcon(newIcon(icon))
    if shortcut is not None:
        if isinstance(shortcut, (list, tuple)):
            a.setShortcuts(shortcut)
        else:
            a.setShortcut(shortcut)
    if tip is not None:
        a.setToolTip(tip)
        a.setStatusTip(tip)
    if slot is not None:
        a.triggered.connect(slot)
    if checkable:
        a.setCheckable(True)
    a.setEnabled(enabled)
    return a


def addActions(widget, actions):
    for action in actions:
        if action is None:
            widget.addSeparator()
        elif isinstance(action, QMenu):
            widget.addMenu(action)
        else:
            widget.addAction(action)


def labelValidator():
    return QRegExpValidator(QRegExp(r'^[^ \t].+'), None)


class struct(object):

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


def distance(p):
    return sqrt(p.x() * p.x() + p.y() * p.y())


def fmtShortcut(text):
    mod, key = text.split('+', 1)
    return '<b>%s</b>+<b>%s</b>' % (mod, key)


def generateColorByText(text):
    s = str(ustr(text))
    hashCode = int(hashlib.sha256(s.encode('utf-8')).hexdigest(), 20)
    r = int((hashCode / 255) % 255)
    g = int((hashCode / 65025)  % 255)
    b = int((hashCode / 16581375)  % 255)
    return QColor(r, g, b, 220)
class Settings(object):
    def __init__(self):
        # Be default, the home will be in the same folder as labelImg
        home = os.path.expanduser("~")
        self.data = {}
        self.path = os.path.join(home, '.labelImgSettings.pkl')

    def __setitem__(self, key, value):
        self.data[key] = value

    def __getitem__(self, key):
        return self.data[key]

    def get(self, key, default=None):
        if key in self.data:
            return self.data[key]
        return default

    def save(self):
        if self.path:
            with open(self.path, 'wb') as f:
                pickle.dump(self.data, f, pickle.HIGHEST_PROTOCOL)
                return True
        return False

    def load(self):
        if os.path.exists(self.path):
            with open(self.path, 'rb') as f:                
                self.data = pickle.load(f)
                return True
        return False

    def reset(self):
        if os.path.exists(self.path):
            os.remove(self.path)
            print ('Remove setting pkl file ${0}'.format(self.path))
        self.data = {}
        self.path = None

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_path = os.path.join("./", 'drive-python-quickstart.json')
    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials



def return_number(imggoc, valueshaps):
    allloca1 = valueshaps
    arrstr = allloca1.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh = imggoc[y1 :y1 + h1, x1:x1 + w1]

    panel = cv2.cvtColor(imgwh, cv2.COLOR_BGR2GRAY)
    ret, thresh5 = cv2.threshold(panel,160,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_Writehand_Number'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_Writehand_Number' 
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 12)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values = _get_string_num(output.values)  
    except:
        pass
    return values  

def return_code(imggoc, valueshaps):
    allloca1 = valueshaps
    arrstr = allloca1.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh = imggoc[y1 :y1 + h1, x1:x1 + w1]

    panel = cv2.cvtColor(imgwh, cv2.COLOR_BGR2GRAY)
    ret, thresh5 = cv2.threshold(panel,160,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_Code_Number'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_Code_Number'
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 10)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values= _get_num(output.values) 
    except:
        pass
    return values    
  
def return_date(imggoc, valueshaps):
    #allloca2 = str(shapes[1][1])
    allloca2 = valueshaps
    arrstr = allloca2.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh2 = imggoc[y1 :y1 + h1, x1:x1 + w1]
    #cv2.imshow("1",imgwh2)
    #cv2.waitKey(0)
    panel2 = cv2.cvtColor(imgwh2, cv2.COLOR_BGR2GRAY)
    #cv2.imshow("1",imgwh2)
    #cv2.waitKey(0)
    ret, thresh5 = cv2.threshold(panel2,150,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel2 = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_Date_Kenshin'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_Date_Kenshin'
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 11)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel2,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values= _get_string_num_Day(output.values) 
                values=values.replace("$",".")
                values=Return_string_values_FieldDay_Goki(values) 
    except:
        pass

    return values

def return_date_monshin(imggoc, valueshaps):
    #allloca2 = str(shapes[1][1])
    allloca2 = valueshaps
    arrstr = allloca2.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh2 = imggoc[y1 :y1 + h1, x1:x1 + w1]
    #cv2.imshow("1",imgwh2)
    #cv2.waitKey(0)
    panel2 = cv2.cvtColor(imgwh2, cv2.COLOR_BGR2GRAY)
    #cv2.imshow("1",imgwh2)
    #cv2.waitKey(0)
    ret, thresh5 = cv2.threshold(panel2,150,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel2 = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_Date_Monshin'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_Date_Monshin'
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 11)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel2,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values= _get_string_num_Day(output.values) 
                values=values.replace("$",".")
                values=Return_string_values_FieldDay_Goki(values) 
    except:
        pass
    return values

def return_kana(imggoc, valueshaps):
    allloca3 = valueshaps
    arrstr = allloca3.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh3 = imggoc[y1 :y1 + h1, x1:x1 + w1]

    panel3 = cv2.cvtColor(imgwh3, cv2.COLOR_BGR2GRAY)
    ret, thresh5 = cv2.threshold(panel3,160,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel3 = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_Kana'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_Kana'
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 84)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel3,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values = _get_string(output.values)  
                values= return_string(values) 
    except:
        pass

    return values

def return_sdt(imggoc, valueshaps):
    allloca3 = valueshaps
    arrstr = allloca3.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh3 = imggoc[y1 :y1 + h1, x1:x1 + w1]

    panel3 = cv2.cvtColor(imgwh3, cv2.COLOR_BGR2GRAY)
    ret, thresh5 = cv2.threshold(panel3,150,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel3 = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_Phone_Number'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_Phone_Number'
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 11)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel3,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values= _get_string_sdt(output.values)   
                if values=="043-259528":
                    values=""  
    except:
        pass

    return values

def return_truefalse(imggoc, valueshaps):
    allloca3 = valueshaps
    arrstr = allloca3.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh3 = imggoc[y1 :y1 + h1, x1:x1 + w1]

    panel3 = cv2.cvtColor(imgwh3, cv2.COLOR_BGR2GRAY)
    ret, thresh5 = cv2.threshold(panel3,127,255,1)
    #thresh2 = cv2.dilate(thresh5,kernel5,iterations = 1)
    panel3 = cv2.bitwise_not(thresh5)
    values = '---'
    try:
        tf.reset_default_graph()
        strvalues = ""         
        #arr = folder.split('/')
        #arr[len(arr) - 1] = 'model_True_False'
        #FLAGS.model = '/'.join(arr)
        FLAGS.model = foldermodel + '/model_True_False'
        with tf.Graph().as_default():
            image,width = _get_input() # Placeholder tensors C:\Users\Tool\Desktop\OCR_Kenshin
            proc_image = _preprocess_image(image)
            proc_image = tf.reshape(proc_image,[1,32,-1,1]) # Make first dim batch

            with tf.device(FLAGS.device):
                features,sequence_length = convnet_layers(proc_image, width, mode)
                logits = rnn_layers(features, sequence_length, 6)
                prediction = _get_output(logits,sequence_length)

            session_config = _get_session_config()
            restore_model = _get_init_trained()
            
            init_op = tf.group(tf.global_variables_initializer(),
                                tf.local_variables_initializer()) 
            with tf.Session(config=session_config) as sess:  
                sess.run(init_op)
                restore_model(sess, _get_checkpoint(FLAGS)) # Get latest checkpoint
                img = np.stack((panel3,) * 3, -1)
                r = 31.0 / img.shape[0]
                dim = (int(img.shape[1] * r), 31)
                img_data = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
                imagenp = np.array(img_data)
                imagenp = imagenp[:,:,:1] 
                [output] = sess.run(prediction,{ image: imagenp, width: imagenp.shape[1] }) 
                values= _get_string_tracnghiem(output.values)
    except:
        pass

    return values


#region google_new
def localize_objects(path):
    """Localize objects in the local image.

    Args:
    path: The path to the local file.
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()

    with open(path, 'rb') as image_file:
        content = image_file.read()
    image = vision.types.Image(content=content)

    objects = client.object_localization(
        image=image).localized_object_annotations

    print('Number of objects found: {}'.format(len(objects)))
    for object_ in objects:
        print('\n{} (confidence: {})'.format(object_.name, object_.score))
        print('Normalized bounding polygon vertices: ')
        for vertex in object_.bounding_poly.normalized_vertices:
            print(' - ({}, {})'.format(vertex.x, vertex.y))
# [END vision_localize_objects_beta]


# [START vision_localize_objects_gcs_beta]
def localize_objects_uri(uri):
    """Localize objects in the image on Google Cloud Storage

    Args:
    uri: The path to the file in Google Cloud Storage (gs://...)
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()

    image = vision.types.Image()
    image.source.image_uri = uri

    objects = client.object_localization(
        image=image).localized_object_annotations

    print('Number of objects found: {}'.format(len(objects)))
    for object_ in objects:
        print('\n{} (confidence: {})'.format(object_.name, object_.score))
        print('Normalized bounding polygon vertices: ')
        for vertex in object_.bounding_poly.normalized_vertices:
            print(' - ({}, {})'.format(vertex.x, vertex.y))
# [END vision_localize_objects_gcs_beta]


# [START vision_handwritten_ocr_beta]
# [END vision_handwritten_ocr_beta]


# [START vision_handwritten_ocr_gcs_beta]
def detect_handwritten_ocr_uri(uri):
    """Detects handwritten characters in the file located in Google Cloud
    Storage.

    Args:
    uri: The path to the file in Google Cloud Storage (gs://...)
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image()
    image.source.image_uri = uri

    # Language hint codes for handwritten OCR:
    # en-t-i0-handwrit, mul-Latn-t-i0-handwrit
    # Note: Use only one language hint code per request for handwritten OCR.
    image_context = vision.types.ImageContext(
        language_hints=['ja-t-i0-handwrit'])

    response = client.document_text_detection(image=image,
                                              image_context=image_context)

    print('Full Text: {}'.format(response.full_text_annotation.text))
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block.paragraphs:
                print('Paragraph confidence: {}'.format(
                    paragraph.confidence))

                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    print('Word text: {} (confidence: {})'.format(
                        word_text, word.confidence))

                    for symbol in word.symbols:
                        print('\tSymbol: {} (confidence: {})'.format(
                            symbol.text, symbol.confidence))
# [END vision_handwritten_ocr_gcs_beta]
#endregion google_new\
#region readgoogle
#chu in
def detect_text(path):
    """Detects text in the file."""
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    # [START vision_python_migration_text_detection]
    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    abc=format(response.full_text_annotation.text)
    return abc
#endregion redgoogle

#region google_new
def localize_objects(path):
    """Localize objects in the local image.

    Args:
    path: The path to the local file.
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()

    with open(path, 'rb') as image_file:
        content = image_file.read()
    image = vision.types.Image(content=content)

    objects = client.object_localization(
        image=image).localized_object_annotations

    print('Number of objects found: {}'.format(len(objects)))
    for object_ in objects:
        print('\n{} (confidence: {})'.format(object_.name, object_.score))
        print('Normalized bounding polygon vertices: ')
        for vertex in object_.bounding_poly.normalized_vertices:
            print(' - ({}, {})'.format(vertex.x, vertex.y))
# [END vision_localize_objects_beta]


# [START vision_localize_objects_gcs_beta]
def localize_objects_uri(uri):
    """Localize objects in the image on Google Cloud Storage

    Args:
    uri: The path to the file in Google Cloud Storage (gs://...)
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()

    image = vision.types.Image()
    image.source.image_uri = uri

    objects = client.object_localization(
        image=image).localized_object_annotations

    print('Number of objects found: {}'.format(len(objects)))
    for object_ in objects:
        print('\n{} (confidence: {})'.format(object_.name, object_.score))
        print('Normalized bounding polygon vertices: ')
        for vertex in object_.bounding_poly.normalized_vertices:
            print(' - ({}, {})'.format(vertex.x, vertex.y))
# [END vision_localize_objects_gcs_beta]


# [START vision_handwritten_ocr_beta]
def detect_handwritten_ocr(path):
    """Detects handwritten characters in a local image.

    Args:
    path: The path to the local file.
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    # Language hint codes for handwritten OCR:
    # en-t-i0-handwrit, mul-Latn-t-i0-handwrit
    # Note: Use only one language hint code per request for handwritten OCR.
    image_context = vision.types.ImageContext(
        language_hints=['ja-t-i0-handwrit'])

    response = client.document_text_detection(image=image,
                                              image_context=image_context)

    abc='Full Text: {}'.format(response.full_text_annotation.text)
    return format(response.full_text_annotation.text)

# [END vision_handwritten_ocr_beta]


# [START vision_handwritten_ocr_gcs_beta]
def detect_handwritten_ocr_uri(uri):
    """Detects handwritten characters in the file located in Google Cloud
    Storage.

    Args:
    uri: The path to the file in Google Cloud Storage (gs://...)
    """
    from google.cloud import vision_v1p3beta1 as vision
    client = vision.ImageAnnotatorClient()
    image = vision.types.Image()
    image.source.image_uri = uri

    # Language hint codes for handwritten OCR:
    # en-t-i0-handwrit, mul-Latn-t-i0-handwrit
    # Note: Use only one language hint code per request for handwritten OCR.
    image_context = vision.types.ImageContext(
        language_hints=['ja-t-i0-handwrit'])

    response = client.document_text_detection(image=image,
                                              image_context=image_context)

    print('Full Text: {}'.format(response.full_text_annotation.text))
    for page in response.full_text_annotation.pages:
        for block in page.blocks:
            print('\nBlock confidence: {}\n'.format(block.confidence))

            for paragraph in block.paragraphs:
                print('Paragraph confidence: {}'.format(
                    paragraph.confidence))

                for word in paragraph.words:
                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])
                    print('Word text: {} (confidence: {})'.format(
                        word_text, word.confidence))

                    for symbol in word.symbols:
                        print('\tSymbol: {} (confidence: {})'.format(
                            symbol.text, symbol.confidence))
# [END vision_handwritten_ocr_gcs_beta]
#endregion google_new\
#region readgoogle
def OCR_Google_new(imggoc, valueshaps):
    allloca3 = valueshaps
    arrstr = allloca3.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh3 = imggoc[y1 :y1 + h1, x1:x1 + w1]

    arr = folder.split('/')
    arr[len(arr) - 1] = '1.jpg'
    pathimage = '/'.join(arr)
    arr = folder.split('/')
    arr[len(arr) - 1] = '1.txt'
    pathtxt = '/'.join(arr)

    cv2.imwrite(pathimage,imgwh3)
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(dest='command')

    object_parser = subparsers.add_parser(
        'object-localization', help=localize_objects.__doc__)
    object_parser.add_argument('path')

    object_uri_parser = subparsers.add_parser(
        'object-localization-uri', help=localize_objects_uri.__doc__)
    object_uri_parser.add_argument('uri')

    handwritten_parser = subparsers.add_parser(
        'handwritten-ocr', help=detect_handwritten_ocr.__doc__)
    handwritten_parser.add_argument('path')

    handwritten_uri_parser = subparsers.add_parser(
        'handwritten-ocr-uri', help=detect_handwritten_ocr_uri.__doc__)
    handwritten_uri_parser.add_argument('uri')

    #strvalues=detect_handwritten_ocr(pathimage)
    strvalues=detect_text(pathimage)
    return strvalues
#chu in
def detect_text(path):
    """Detects text in the file."""
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    # [START vision_python_migration_text_detection]
    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    abc=format(response.full_text_annotation.text)
    return abc

def OCR_Google_WH(imggoc, valueshaps):
    allloca3 = valueshaps
    arrstr = allloca3.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh3 = imggoc[y1 :y1 + h1, x1:x1 + w1]

    arr = folder.split('/')
    arr[len(arr) - 1] = '1.jpg'
    pathimage = '/'.join(arr)
    arr = folder.split('/')
    arr[len(arr) - 1] = '1.txt'
    pathtxt = '/'.join(arr)

    cv2.imwrite(pathimage,imgwh3)
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(dest='command')

    object_parser = subparsers.add_parser(
        'object-localization', help=localize_objects.__doc__)
    object_parser.add_argument('path')

    object_uri_parser = subparsers.add_parser(
        'object-localization-uri', help=localize_objects_uri.__doc__)
    object_uri_parser.add_argument('uri')

    handwritten_parser = subparsers.add_parser(
        'handwritten-ocr', help=detect_handwritten_ocr.__doc__)
    handwritten_parser.add_argument('path')

    handwritten_uri_parser = subparsers.add_parser(
        'handwritten-ocr-uri', help=detect_handwritten_ocr_uri.__doc__)
    handwritten_uri_parser.add_argument('uri')

    strvalues=detect_handwritten_ocr(pathimage)
    #strvalues=detect_text(pathimage)
    return strvalues
#chu in
def detect_text(path):
    """Detects text in the file."""
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    # [START vision_python_migration_text_detection]
    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    abc=format(response.full_text_annotation.text)
    return abc

def Return_OCR_Google(imggoc, valueshaps):

    allloca3 = valueshaps
    arrstr = allloca3.split(')')
    x1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[0])
    y1 = int(arrstr[0].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1])
    w1 = int(arrstr[1].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[1]) - x1
    h1 = int(arrstr[2].replace('(','').replace('[','').replace(']','').replace(' ','').split(',')[2]) - y1
    imgwh3 = imggoc[y1 :y1 + h1, x1:x1 + w1]

    arr = folder.split('/')
    arr[len(arr) - 1] = '1.jpg'
    pathimage = '/'.join(arr)
    arr = folder.split('/')
    arr[len(arr) - 1] = '1.txt'
    pathtxt = '/'.join(arr)

    cv2.imwrite(pathimage,imgwh3)

    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)
    #imgfile = 'C:/Users/Tool/Desktop/HAJIME/GOOGLE/Chiba/17.jpg'  # Image with texts (png, jpg, bmp, gif, pdf)
    #txtfile = 'C:/Users/Tool/Desktop/HAJIME/GOOGLE/Chiba/17.txt'  # Text file outputted by OCR
    #folder2="C:/Users/Tool/Desktop/New folder (2)/AI_Handwriting/PythonApplication1/PythonApplication1/OCR_ChuIN/ImageSave"
    imgfile = pathimage  # Image with texts (png, jpg, bmp, gif, pdf)

    mime = 'application/vnd.google-apps.document'
    res = service.files().create(
        body={
            'name': imgfile,
            'mimeType': mime
        },
        media_body=MediaFileUpload(pathimage, mimetype=mime, resumable=True)
    ).execute()

    downloader = MediaIoBaseDownload(
        io.FileIO(pathtxt, 'wb'),
        service.files().export_media(fileId=res['id'], mimeType="text/plain")
    )
    done = False
    while done is False:
        status, done = downloader.next_chunk()

    service.files().delete(fileId=res['id']).execute()

    os.remove(pathimage)
    with codecs.open(pathtxt, 'r', 'UTF-8') as lines:
        strvalues1=""
        for item in lines:
            if item.replace("\n","").replace("\r\n","")!="":
                strvalues1=strvalues1+item+"\n"
        lines.close()
    #strvalues=re.sub(r, r"\1", strvalues)
    #strvalues1=strvalues1.replace("-","").replace("_","").replace(","," ").replace("  "," ").replace("|","").replace("「","").strip()
    strvalues1=strvalues1.replace("-","").replace("_","").replace(","," ").replace("|","").replace("「","").strip()
    strvalues1=strvalues1.replace("\r\n","").replace("\n\n","")
    try:
        os.remove(pathtxt)
    except:
        pass
    return strvalues1

def Return_string_values_FieldDay_Goki(values):
    try:
        values=values.replace(".","")
        txtall=""
        Yeard=values[:2]
        Month=values[2:4]
        Day=values[4:]
        imonth=int(Month)
        iday=int(Day)
        iyeard=int(Yeard)
        if iday>30 or imonth>12:
            return ""
        else:
            txtall=str(iyeard)+"/"+str(Month)+"/"+str(Day)
            return txtall  
    except:
        pass 
def conv_layer(bottom, params, training):
    """Build a convolutional layer using entry from layer_params)"""

    batch_norm = params[4] # Boolean
    if batch_norm:
        activation = None
    else:
        activation = tf.nn.relu

    kernel_initializer = tf.contrib.layers.variance_scaling_initializer()
    bias_initializer = tf.constant_initializer(value=0.0)
    top = tf.layers.conv2d(bottom, 
                           filters=params[0],
                           kernel_size=params[1],
                           padding=params[2],
                           activation=activation,
                           kernel_initializer=kernel_initializer,
                           bias_initializer=bias_initializer,
                           name=params[3])
    if batch_norm:
        top = norm_layer(top, training, params[3] + '/batch_norm')
        top = tf.nn.relu(top, name=params[3] + '/relu')

    return top

def pool_layer(bottom, wpool, padding, name):
    """Short function to build a pooling layer with less syntax"""
    top = tf.layers.max_pooling2d(bottom, 2, [2,wpool], 
                                   padding=padding, 
                                   name=name)
    return top

def norm_layer(bottom, training, name):
    """Short function to build a batch normalization layer with less syntax"""
    top = tf.layers.batch_normalization(bottom, axis=3, # channels last,
                                         training=training,
                                         name=name)
    return top


def convnet_layers(inputs, widths, mode):
    """Build convolutional network layers attached to the given input tensor"""
    training = (mode == learn.ModeKeys.TRAIN)

    # inputs should have shape [ ?, 32, ?, 1 ]
    with tf.variable_scope("convnet"): # h,w
              
        conv1 = conv_layer(inputs, layer_params[0], training) # 30,30
        conv2 = conv_layer(conv1, layer_params[1], training) # 30,30
        pool2 = pool_layer(conv2, 2, 'valid', 'pool2')        # 15,15
        conv3 = conv_layer(pool2, layer_params[2], training) # 15,15
        conv4 = conv_layer(conv3, layer_params[3], training) # 15,15
        pool4 = pool_layer(conv4, 1, 'valid', 'pool4')       # 7,14
        conv5 = conv_layer(pool4, layer_params[4], training) # 7,14
        conv6 = conv_layer(conv5, layer_params[5], training) # 7,14
        pool6 = pool_layer(conv6, 1, 'valid', 'pool6')        # 3,13
        conv7 = conv_layer(pool6, layer_params[6], training) # 3,13
        conv8 = conv_layer(conv7, layer_params[7], training) # 3,13
        pool8 = tf.layers.max_pooling2d(conv8, [3,1], [3,1], 
                                   padding='valid', name='pool8') # 1,13
        features = tf.squeeze(pool8, axis=1, name='features') # squeeze row dim
                
        kernel_sizes = [ params[1] for params in layer_params]

        # Calculate resulting sequence length from original image widths
        conv1_trim = tf.constant(2 * (kernel_sizes[0] // 2),
                                  dtype=tf.int32,
                                  name='conv1_trim')
        one = tf.constant(1, dtype=tf.int32, name='one')
        two = tf.constant(2, dtype=tf.int32, name='two')
        after_conv1 = tf.subtract(widths, conv1_trim)
        after_pool2 = tf.floor_div(after_conv1, two)
        after_pool4 = tf.subtract(after_pool2, one)
        after_pool6 = tf.subtract(after_pool4, one) 
        after_pool8 = after_pool6
        
        sequence_length = tf.reshape(after_pool8,[-1], name='seq_len') # Vectorize

        return features,sequence_length

def rnn_layer(bottom_sequence,sequence_length,rnn_size,scope):
    """Build bidirectional (concatenated output) RNN layer"""

    weight_initializer = tf.truncated_normal_initializer(stddev=0.01)
    
    # Default activation is tanh
    cell_fw = tf.contrib.rnn.LSTMCell(rnn_size, 
                                       initializer=weight_initializer)
    cell_bw = tf.contrib.rnn.LSTMCell(rnn_size, 
                                       initializer=weight_initializer)
    # Include?
    #cell_fw = tf.contrib.rnn.DropoutWrapper( cell_fw,
    #                                         input_keep_prob=dropout_rate )
    #cell_bw = tf.contrib.rnn.DropoutWrapper( cell_bw,
    #                                         input_keep_prob=dropout_rate )
    
    rnn_output,_ = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw, bottom_sequence,
        sequence_length=sequence_length,
        time_major=True,
        dtype=tf.float32,
        scope=scope)
    
    # Concatenation allows a single output op because [A B]*[x;y] = Ax+By
    # [ paddedSeqLen batchSize 2*rnn_size]
    rnn_output_stack = tf.concat(rnn_output,2,name='output_stack')
    
    return rnn_output_stack


def rnn_layers(features, sequence_length, num_classes):
    """Build a stack of RNN layers from input features"""

    # Input features is [batchSize paddedSeqLen numFeatures]
    logit_activation = tf.nn.relu
    weight_initializer = tf.contrib.layers.variance_scaling_initializer()
    bias_initializer = tf.constant_initializer(value=0.0)

    with tf.variable_scope("rnn"):
        # Transpose to time-major order for efficiency
        rnn_sequence = tf.transpose(features, perm=[1, 0, 2], name='time_major')
        rnn1 = rnn_layer(rnn_sequence, sequence_length, rnn_size, 'bdrnn1')
        rnn2 = rnn_layer(rnn1, sequence_length, rnn_size, 'bdrnn2')
        rnn_logits = tf.layers.dense(rnn2, num_classes + 1, 
                                      activation=logit_activation,
                                      kernel_initializer=weight_initializer,
                                      bias_initializer=bias_initializer,
                                      name='logits')
        return rnn_logits
    

def ctc_loss_layer(rnn_logits, sequence_labels, sequence_length):
    """Build CTC Loss layer for training"""
    loss = tf.nn.ctc_loss(sequence_labels, rnn_logits, sequence_length,
                           time_major=True)
    total_loss = tf.reduce_mean(loss)
    return total_loss


def _get_image(filename):
    """Load image data for placement in graph"""
    #image = Image.open(filename)
    #image = np.array(image)
    image = np.array(filename)
    # in mjsynth, all three channels are the same in these grayscale-cum-RGB
    # data
    image = image[:,:,:1] # so just extract first channel, preserving 3D shape

    return image


def _preprocess_image(image):

    # Copied from mjsynth.py.  Should be abstracted to a more general module.
    
    # Rescale from uint8([0,255]) to float([-0.5,0.5])
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.subtract(image, 0.5)

    # Pad with copy of first row to expand to 32 pixels height
    first_row = tf.slice(image, [0, 0, 0], [1, -1, -1])
    image = tf.concat([first_row, image], 0)

    return image


def _get_input():
    """Set up and return image and width placeholder tensors"""

    # Raw image as placeholder to be fed one-by-one by dictionary
    image = tf.placeholder(tf.uint8, shape=[31, None, 1])
    width = tf.placeholder(tf.int32,shape=[]) # for ctc_loss

    return image,width


def _get_output(rnn_logits,sequence_length):
    """Create ops for validation
       predictions: Results of CTC beacm search decoding
    """
    with tf.name_scope("test"):
        predictions,_ = tf.nn.ctc_beam_search_decoder(rnn_logits, 
                                                   sequence_length,
                                                   beam_width=128,
                                                   top_paths=1,
                                                   merge_repeated=False)

    return predictions


def _get_session_config():
    """Setup session config to soften device placement"""
    config = tf.ConfigProto(allow_soft_placement=True, 
        log_device_placement=False)

    return config


def _get_checkpoint(FLAGS):
    """Get the checkpoint path from the given model output directory"""
    ckpt = tf.train.get_checkpoint_state(FLAGS.model)

    if ckpt and ckpt.model_checkpoint_path:
        ckpt_path = ckpt.model_checkpoint_path
    else:
        raise RuntimeError('No checkpoint file found')

    return ckpt_path


def _get_init_trained():
    """Return init function to restore trained model from a given checkpoint"""
    saver_reader = tf.train.Saver(tf.get_collection(tf.GraphKeys.GLOBAL_STEP) + tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES))
    
    init_fn = lambda sess,ckpt_path: saver_reader.restore(sess, ckpt_path)
    return init_fn

def _get_string(labels):
    """Transform an 1D array of labels into the corresponding character string"""
    string = ''.join([out_charset2[c] for c in labels])
    return string

#endregion
def _get_string_sdt(labels):
    """Transform an 1D array of labels into the corresponding character string"""
    out_charset = "-0123456789"
    string = ''.join([out_charset[c] for c in labels])
    return string
def _get_num(labels):
    """Transform an 1D array of labels into the corresponding character string"""
    string = ''.join([out_charsetnum[c] for c in labels])
    return string
def _get_string_num(labels):
    """Transform an 1D array of labels into the corresponding character string"""
    out_charset = "$@0123456789"
    string = ''.join([out_charset[c] for c in labels])
    return string
def _get_string_tracnghiem(labels):
    """Transform an 1D array of labels into the corresponding character string"""
    out_charset = "012345"
    string = ''.join([out_charset[c] for c in labels])
    return string
def _get_string_num_Day(labels):
    """Transform an 1D array of labels into the corresponding character string"""
    out_charset = "$0123456789"
    string = ''.join([out_charset[c] for c in labels])
    return string
#
out_charset2 = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&X()-+0123456789{}`~}}}}}"
out_charset = "　コシバセイゴジツカワミヨキヤマムラゲホウサオユヒグズエソメノナモトダュケスチハタンクルロレガベリフアブョギゼニヌヅドデビボテザネヘッャィポペァパプピヂォゾゥェヲヴヱー"
#test model kana
#out_charset = '
#タカハシケサクダミツルモトマイエユキコフノブオリヒラジナチワヨアソウゾヤンロズュゴボメニムセテベレデスザョドガネゲビバギヅグゼヘホヌッーペャヂポパィェァピォゥプヲ'
#out_charset2 = '
#abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&X()-+0123456789{}`~}}}'

#out_charset=" ﾟﾞｧｱｨｲｩｳｪｴｫｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂｯｯﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓｬﾔｭﾕｮﾖﾗﾘﾙﾚﾛﾜｦﾝｰ"
#out_charset2=" abcdefghijklmnopqrstuvwxyz!@#$%^&X()ABCDEFGHIJKLMNOPQRSTUVW"
out_charsetnum = "0123456789"

def return_string(strresult):
    for i in range(len(out_charset)):
        strresult = strresult.replace(out_charset2[i],out_charset[i])
    return strresult



# Utility functions and classes.
def get_image(image):
    abc = 0

def convertPDF2JPG_multi(filepdf,folder,name):
    #used to generate temp file name.  so we will not duplicate or replace
    #anything
    uuid_set = str(uuid.uuid4().fields[-1])[:5]
    try:
        #create folder
        if not os.path.exists(folder + "/" + name):
            os.makedirs(folder + "/" + name)
        #now lets convert the PDF to Image
        #this is good resolution As far as I know
        with Image(filename=folder + "/" + filepdf, resolution=200) as img:
            #keep good quality
            img.compression_quality = 80
            #save it to tmp name
            img.save(filename=folder + "/" + name + "/" + name + "%s.jpg" % uuid_set)
    except Exception as e:
        #always keep track the error until the code has been clean
        print(e)
        return False
    else:
        return True
        """
        We finally success to convert pdf to image. 
        but image is not join by it self when we convert pdf files to image. 
        now we need to merge all file
        """
        pathsave = []
        try:
            #search all image in temp path.  file name ends with uuid_set value
            list_im = glob.glob(folder + "/" + name + "/" + name + "%s.jpg" % uuid_set)
            list_im.sort() #sort the file before joining it
            imgs = [Img.open(i) for i in list_im]
            #now lets Combine several images vertically with Python
            min_shape = sorted([(np.sum(i.size), i.size) for i in imgs])[0][1]
            imgs_comb = np.vstack((np.asarray(i.resize(min_shape)) for i in imgs))
            # for horizontally change the vstack to hstack
            imgs_comb = Img.fromarray(imgs_comb)
            pathsave = "MyPdf%s.jpg" % uuid_set
            print(pathsave)
            #now save the image
            imgs_comb.save(pathsave)
            #and then remove all temp image
            for i in list_im:
                os.remove(i)
        except Exception as e:
            print(e) 
            return False
        return pathsave
def convertPDF2JPG(filepdf,folder,name):
    #used to generate temp file name.  so we will not duplicate or replace
    #anything
    uuid_set = str(uuid.uuid4().fields[-1])[:5]

    #dst_pdf = PyPDF2.PdfFileWriter()
    #dst_pdf.addPage(PyPDF2.PdfFileReader(folder+"/"+filepdf).getPage(0))

    #pdf_bytes = io.BytesIO()
    #dst_pdf.write(pdf_bytes)
    #pdf_bytes.seek(0)
    try:
        #if not os.path.exists(folder + "/" + name):
        #    os.makedirs(folder + "/" + name)
        #now lets convert the PDF to Image
        #this is good resolution As far as I know
        fileopen = folder + "/" + filepdf
        with Image(filename=fileopen, resolution=200) as img:
            #keep good quality
            img.compression_quality = 80
            #print(len(img))
            #save it to tmp name
            #print(uuid_set)
            img.save(filename=folder + "/" + username + "/" + name + "-%s.jpg" % uuid_set)
    except Exception as e:
        #always keep track the error until the code has been clean
        print(e)
        return False
    #else:
    #    return True
        """
        We finally success to convert pdf to image. 
        but image is not join by it self when we convert pdf files to image. 
        now we need to merge all file
        """
    list_im = []
    try:
        list_im = [f for f in os.listdir(folder + "/" + username) if f.endswith(".jpg")]
        ###search all image in temp path.  file name ends with uuid_set value
        #list_im = glob.glob(folder + "/" + name + "/" + name + "%s.jpg" %
        #uuid_set)
        #list_im.sort() #sort the file before joining it
    except Exception as e:
        print(e) 
        return False
    return list_im

def have_qstring():
    '''p3/qt5 get rid of QString wrapper as py3 has native unicode str type'''
    return not (sys.version_info.major >= 3 or QT_VERSION_STR.startswith('5.'))

def util_qt_strlistclass():
    return QStringList if have_qstring() else list


class WindowMixin(object):

    def menu(self, title, actions=None):
        menu = self.menuBar().addMenu(title)
        if actions:
            addActions(menu, actions)
        return menu

    def toolbar(self, title, actions=None):
        toolbar = ToolBar(title)
        toolbar.setObjectName(u'%sToolBar' % title)
        # toolbar.setOrientation(Qt.Vertical)
        toolbar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        if actions:
            addActions(toolbar, actions)
        self.addToolBar(Qt.LeftToolBarArea, toolbar)
        return toolbar


# PyQt5: TypeError: unhashable type: 'QListWidgetItem'
class HashableQListWidgetItem(QListWidgetItem):

    def __init__(self, *args):
        super(HashableQListWidgetItem, self).__init__(*args)

    def __hash__(self):
        return hash(id(self))

CURSOR_DEFAULT = Qt.ArrowCursor
CURSOR_POINT = Qt.PointingHandCursor
CURSOR_DRAW = Qt.CrossCursor
CURSOR_MOVE = Qt.ClosedHandCursor
CURSOR_GRAB = Qt.OpenHandCursor

# class Canvas(QGLWidget):


class Canvas(QWidget):
    zoomRequest = pyqtSignal(int)
    scrollRequest = pyqtSignal(int, int)
    newShape = pyqtSignal()
    createShape = pyqtSignal()
    openNextImg = pyqtSignal()
    selectionChanged = pyqtSignal(bool)
    shapeMoved = pyqtSignal()
    drawingPolygon = pyqtSignal(bool)

    CREATE, EDIT = list(range(2))

    epsilon = 11.0

    def __init__(self, *args, **kwargs):
        super(Canvas, self).__init__(*args, **kwargs)
        # Initialise local state.
        self.mode = self.EDIT
        self.shapes = []
        self.current = None
        self.selectedShape = None  # save the selected shape here
        self.selectedShapeCopy = None
        self.drawingLineColor = QColor(0, 0, 255)
        self.drawingRectColor = QColor(0, 0, 255) 
        self.line = Shape(line_color=self.drawingLineColor)
        self.prevPoint = QPointF()
        self.offsets = QPointF(), QPointF()
        self.scale = 1.0
        self.pixmap = QPixmap()
        self.visible = {}
        self._hideBackround = False
        self.hideBackround = False
        self.hShape = None
        self.hVertex = None
        self._painter = QPainter()
        self._cursor = CURSOR_DEFAULT
        # Menus:
        self.menus = (QMenu(), QMenu())
        # Set widget options.
        self.setMouseTracking(True)
        self.setFocusPolicy(Qt.WheelFocus)
        self.verified = False

    def setDrawingColor(self, qColor):
        self.drawingLineColor = qColor
        self.drawingRectColor = qColor

    def enterEvent(self, ev):
        self.overrideCursor(self._cursor)

    def leaveEvent(self, ev):
        self.restoreCursor()

    def focusOutEvent(self, ev):
        self.restoreCursor()

    def isVisible(self, shape):
        return self.visible.get(shape, True)

    def drawing(self):
        return self.mode == self.CREATE

    def editing(self):
        return self.mode == self.EDIT

    def setEditing(self, value=True):
        self.mode = self.EDIT if value else self.CREATE
        if not value:  # Create
            self.unHighlight()
            self.deSelectShape()
        self.prevPoint = QPointF()
        self.repaint()

    def unHighlight(self):
        if self.hShape:
            self.hShape.highlightClear()
        self.hVertex = self.hShape = None

    def selectedVertex(self):
        return self.hVertex is not None

    def mouseMoveEvent(self, ev):
        """Update line with last point and current coordinates."""
        pos = self.transformPos(ev.pos())

        # Update coordinates in status bar if image is opened
        window = self.parent().window()
        if window.filePath is not None:
            self.parent().window().labelCoordinates.setText(
                'X: %d; Y: %d' % (pos.x(), pos.y()))

        # Polygon drawing.
        if self.drawing():
            self.overrideCursor(CURSOR_DRAW)
            if self.current:
                color = self.drawingLineColor
                if self.outOfPixmap(pos):
                    # Don't allow the user to draw outside the pixmap.
                    # Project the point to the pixmap's edges.
                    pos = self.intersectionPoint(self.current[-1], pos)
                elif len(self.current) > 1 and self.closeEnough(pos, self.current[0]):
                    # Attract line to starting point and colorise to alert the
                    # user:
                    pos = self.current[0]
                    color = self.current.line_color
                    self.overrideCursor(CURSOR_POINT)
                    self.current.highlightVertex(0, Shape.NEAR_VERTEX)
                self.line[1] = pos
                self.line.line_color = color
                self.prevPoint = QPointF()
                self.current.highlightClear()
            else:
                self.prevPoint = pos
            self.repaint()
            return

        # Polygon copy moving.
        if Qt.RightButton & ev.buttons():
            if self.selectedShapeCopy and self.prevPoint:
                self.overrideCursor(CURSOR_MOVE)
                self.boundedMoveShape(self.selectedShapeCopy, pos)
                self.repaint()
            elif self.selectedShape:
                self.selectedShapeCopy = self.selectedShape.copy()
                self.repaint()
            return

        # Polygon/Vertex moving.
        if Qt.LeftButton & ev.buttons():
            if self.selectedVertex():
                self.boundedMoveVertex(pos)
                self.shapeMoved.emit()
                self.repaint()
            elif self.selectedShape and self.prevPoint:
                self.overrideCursor(CURSOR_MOVE)
                self.boundedMoveShape(self.selectedShape, pos)
                self.shapeMoved.emit()
                self.repaint()
            return

        # Just hovering over the canvas, 2 posibilities:
        # - Highlight shapes
        # - Highlight vertex
        # Update shape/vertex fill and tooltip value accordingly.
        self.setToolTip("Image")
        for shape in reversed([s for s in self.shapes if self.isVisible(s)]):
            # Look for a nearby vertex to highlight. If that fails,
            # check if we happen to be inside a shape.
            index = shape.nearestVertex(pos, self.epsilon)
            if index is not None:
                if self.selectedVertex():
                    self.hShape.highlightClear()
                self.hVertex, self.hShape = index, shape
                shape.highlightVertex(index, shape.MOVE_VERTEX)
                self.overrideCursor(CURSOR_POINT)
                self.setToolTip("Click & drag to move point")
                self.setStatusTip(self.toolTip())
                self.update()
                break
            elif shape.containsPoint(pos):
                if self.selectedVertex():
                    self.hShape.highlightClear()
                self.hVertex, self.hShape = None, shape
                self.setToolTip(
                    "Click & drag to move shape '%s'" % shape.label)
                self.setStatusTip(self.toolTip())
                self.overrideCursor(CURSOR_GRAB)
                self.update()
                break
        else:  # Nothing found, clear highlights, reset state.
            if self.hShape:
                self.hShape.highlightClear()
                self.update()
            self.hVertex, self.hShape = None, None
            self.overrideCursor(CURSOR_DEFAULT)

    def mousePressEvent(self, ev):
        pos = self.transformPos(ev.pos())

        if ev.button() == Qt.LeftButton:
            if self.drawing():
                self.handleDrawing(pos)
            else:
                self.selectShapePoint(pos)
                self.prevPoint = pos
                self.repaint()
        elif ev.button() == Qt.RightButton and self.editing():
            self.selectShapePoint(pos)
            self.prevPoint = pos
            self.repaint()

    def mouseReleaseEvent(self, ev):
        if ev.button() == Qt.RightButton:
            menu = self.menus[bool(self.selectedShapeCopy)]
            self.restoreCursor()
            if not menu.exec_(self.mapToGlobal(ev.pos()))\
               and self.selectedShapeCopy:
                # Cancel the move by deleting the shadow copy.
                self.selectedShapeCopy = None
                self.repaint()
        elif ev.button() == Qt.LeftButton and self.selectedShape:
            if self.selectedVertex():
                self.overrideCursor(CURSOR_POINT)
            else:
                self.overrideCursor(CURSOR_GRAB)
        elif ev.button() == Qt.LeftButton:
            pos = self.transformPos(ev.pos())
            if self.drawing():
                self.handleDrawing(pos)
                self.createShape.emit()
                if(len(self.shapes)==13):
                    self.openNextImg.emit()

    def endMove(self, copy=False):
        assert self.selectedShape and self.selectedShapeCopy
        shape = self.selectedShapeCopy
        #del shape.fill_color
        #del shape.line_color
        if copy:
            self.shapes.append(shape)
            self.selectedShape.selected = False
            self.selectedShape = shape
            self.repaint()
        else:
            self.selectedShape.points = [p for p in shape.points]
        self.selectedShapeCopy = None

    def hideBackroundShapes(self, value):
        self.hideBackround = value
        if self.selectedShape:
            # Only hide other shapes if there is a current selection.
            # Otherwise the user will not be able to select a shape.
            self.setHiding(True)
            self.repaint()

    def handleDrawing(self, pos):
        if self.current and self.current.reachMaxPoints() is False:
            initPos = self.current[0]
            minX = initPos.x()
            minY = initPos.y()
            targetPos = self.line[1]
            maxX = targetPos.x()
            maxY = targetPos.y()
            self.current.addPoint(QPointF(maxX, minY))
            self.current.addPoint(targetPos)
            self.current.addPoint(QPointF(minX, maxY))
            self.finalise()
        elif not self.outOfPixmap(pos):
            self.current = Shape()
            self.current.addPoint(pos)
            self.line.points = [pos, pos]
            self.setHiding()
            self.drawingPolygon.emit(True)
            self.update()

    def setHiding(self, enable=True):
        self._hideBackround = self.hideBackround if enable else False

    def canCloseShape(self):
        return self.drawing() and self.current and len(self.current) > 2

    def mouseDoubleClickEvent(self, ev):
        # We need at least 4 points here, since the mousePress handler
        # adds an extra one before this handler is called.
        if self.canCloseShape() and len(self.current) > 3:
            self.current.popPoint()
            self.finalise()

    def selectShape(self, shape):
        self.deSelectShape()
        shape.selected = True
        self.selectedShape = shape
        self.setHiding()
        self.selectionChanged.emit(True)
        self.update()

    def selectShapePoint(self, point):
        """Select the first shape created which contains this point."""
        self.deSelectShape()
        if self.selectedVertex():  # A vertex is marked for selection.
            index, shape = self.hVertex, self.hShape
            shape.highlightVertex(index, shape.MOVE_VERTEX)
            self.selectShape(shape)
            return
        for shape in reversed(self.shapes):
            if self.isVisible(shape) and shape.containsPoint(point):
                self.selectShape(shape)
                self.calculateOffsets(shape, point)
                return

    def calculateOffsets(self, shape, point):
        rect = shape.boundingRect()
        x1 = rect.x() - point.x()
        y1 = rect.y() - point.y()
        x2 = (rect.x() + rect.width()) - point.x()
        y2 = (rect.y() + rect.height()) - point.y()
        self.offsets = QPointF(x1, y1), QPointF(x2, y2)

    def boundedMoveVertex(self, pos):
        index, shape = self.hVertex, self.hShape
        point = shape[index]
        if self.outOfPixmap(pos):
            pos = self.intersectionPoint(point, pos)

        shiftPos = pos - point
        shape.moveVertexBy(index, shiftPos)

        lindex = (index + 1) % 4
        rindex = (index + 3) % 4
        lshift = None
        rshift = None
        if index % 2 == 0:
            rshift = QPointF(shiftPos.x(), 0)
            lshift = QPointF(0, shiftPos.y())
        else:
            lshift = QPointF(shiftPos.x(), 0)
            rshift = QPointF(0, shiftPos.y())
        shape.moveVertexBy(rindex, rshift)
        shape.moveVertexBy(lindex, lshift)

    def boundedMoveShape(self, shape, pos):
        if self.outOfPixmap(pos):
            return False  # No need to move
        o1 = pos + self.offsets[0]
        if self.outOfPixmap(o1):
            pos -= QPointF(min(0, o1.x()), min(0, o1.y()))
        o2 = pos + self.offsets[1]
        if self.outOfPixmap(o2):
            pos += QPointF(min(0, self.pixmap.width() - o2.x()),
                           min(0, self.pixmap.height() - o2.y()))
        # The next line tracks the new position of the cursor
        # relative to the shape, but also results in making it
        # a bit "shaky" when nearing the border and allows it to
        # go outside of the shape's area for some reason. XXX
        #self.calculateOffsets(self.selectedShape, pos)
        dp = pos - self.prevPoint
        if dp:
            shape.moveBy(dp)
            self.prevPoint = pos
            return True
        return False

    def deSelectShape(self):
        if self.selectedShape:
            self.selectedShape.selected = False
            self.selectedShape = None
            self.setHiding(False)
            self.selectionChanged.emit(False)
            self.update()

    def deleteSelected(self):
        if self.selectedShape:
            shape = self.selectedShape
            self.shapes.remove(self.selectedShape)
            self.selectedShape = None
            self.update()
            return shape

    def copySelectedShape(self):
        if self.selectedShape:
            shape = self.selectedShape.copy()
            self.deSelectShape()
            self.shapes.append(shape)
            shape.selected = True
            self.selectedShape = shape
            self.boundedShiftShape(shape)
            return shape

    def boundedShiftShape(self, shape):
        # Try to move in one direction, and if it fails in another.
        # Give up if both fail.
        point = shape[0]
        offset = QPointF(2.0, 2.0)
        self.calculateOffsets(shape, point)
        self.prevPoint = point
        if not self.boundedMoveShape(shape, point - offset):
            self.boundedMoveShape(shape, point + offset)

    def paintEvent(self, event):
        if not self.pixmap:
            return super(Canvas, self).paintEvent(event)

        p = self._painter
        p.begin(self)
        p.setRenderHint(QPainter.Antialiasing)
        p.setRenderHint(QPainter.HighQualityAntialiasing)
        p.setRenderHint(QPainter.SmoothPixmapTransform)

        p.scale(self.scale, self.scale)
        p.translate(self.offsetToCenter())

        p.drawPixmap(0, 0, self.pixmap)
        Shape.scale = self.scale
        for shape in self.shapes:
            if (shape.selected or not self._hideBackround) and self.isVisible(shape):
                shape.fill = shape.selected or shape == self.hShape
                shape.paint(p)
        if self.current:
            self.current.paint(p)
            self.line.paint(p)
        if self.selectedShapeCopy:
            self.selectedShapeCopy.paint(p)

        # Paint rect
        if self.current is not None and len(self.line) == 2:
            leftTop = self.line[0]
            rightBottom = self.line[1]
            rectWidth = rightBottom.x() - leftTop.x()
            rectHeight = rightBottom.y() - leftTop.y()
            p.setPen(self.drawingRectColor)
            brush = QBrush(Qt.BDiagPattern)
            p.setBrush(brush)
            p.drawRect(leftTop.x(), leftTop.y(), rectWidth, rectHeight)

        if self.drawing() and not self.prevPoint.isNull() and not self.outOfPixmap(self.prevPoint):
            p.setPen(QColor(0, 0, 0))
            p.drawLine(self.prevPoint.x(), 0, self.prevPoint.x(), self.pixmap.height())
            p.drawLine(0, self.prevPoint.y(), self.pixmap.width(), self.prevPoint.y())

        self.setAutoFillBackground(True)
        if self.verified:
            pal = self.palette()
            pal.setColor(self.backgroundRole(), QColor(184, 239, 38, 128))
            self.setPalette(pal)
        else:
            pal = self.palette()
            pal.setColor(self.backgroundRole(), QColor(232, 232, 232, 255))
            self.setPalette(pal)

        p.end()

    def transformPos(self, point):
        """Convert from widget-logical coordinates to painter-logical coordinates."""
        return point / self.scale - self.offsetToCenter()

    def offsetToCenter(self):
        s = self.scale
        area = super(Canvas, self).size()
        w, h = self.pixmap.width() * s, self.pixmap.height() * s
        aw, ah = area.width(), area.height()
        x = (aw - w) / (2 * s) if aw > w else 0
        y = (ah - h) / (2 * s) if ah > h else 0
        return QPointF(x, y)

    def outOfPixmap(self, p):
        w, h = self.pixmap.width(), self.pixmap.height()
        return not (0 <= p.x() <= w and 0 <= p.y() <= h)

    def finalise(self):
        assert self.current
        if self.current.points[0] == self.current.points[-1]:
            self.current = None
            self.drawingPolygon.emit(False)
            self.update()
            return

        self.current.close()
        self.shapes.append(self.current)
        self.current = None
        self.setHiding(False)
        self.newShape.emit()
        self.update()

    def closeEnough(self, p1, p2):
        #d = distance(p1 - p2)
        #m = (p1-p2).manhattanLength()
        # print "d %.2f, m %d, %.2f" % (d, m, d - m)
        return distance(p1 - p2) < self.epsilon

    def intersectionPoint(self, p1, p2):
        # Cycle through each image edge in clockwise fashion,
        # and find the one intersecting the current line segment.
        # http://paulbourke.net/geometry/lineline2d/
        size = self.pixmap.size()
        points = [(0, 0),
                  (size.width(), 0),
                  (size.width(), size.height()),
                  (0, size.height())]
        x1, y1 = p1.x(), p1.y()
        x2, y2 = p2.x(), p2.y()
        d, i, (x, y) = min(self.intersectingEdges((x1, y1), (x2, y2), points))
        x3, y3 = points[i]
        x4, y4 = points[(i + 1) % 4]
        if (x, y) == (x1, y1):
            # Handle cases where previous point is on one of the edges.
            if x3 == x4:
                return QPointF(x3, min(max(0, y2), max(y3, y4)))
            else:  # y3 == y4
                return QPointF(min(max(0, x2), max(x3, x4)), y3)
        return QPointF(x, y)

    def intersectingEdges(self, x1y1, x2y2, points):
        """For each edge formed by `points', yield the intersection
        with the line segment `(x1,y1) - (x2,y2)`, if it exists.
        Also return the distance of `(x2,y2)' to the middle of the
        edge along with its index, so that the one closest can be chosen."""
        x1, y1 = x1y1
        x2, y2 = x2y2
        for i in range(4):
            x3, y3 = points[i]
            x4, y4 = points[(i + 1) % 4]
            denom = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
            nua = (x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)
            nub = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)
            if denom == 0:
                # This covers two cases:
                #   nua == nub == 0: Coincident
                #   otherwise: Parallel
                continue
            ua, ub = nua / denom, nub / denom
            if 0 <= ua <= 1 and 0 <= ub <= 1:
                x = x1 + ua * (x2 - x1)
                y = y1 + ua * (y2 - y1)
                m = QPointF((x3 + x4) / 2, (y3 + y4) / 2)
                d = distance(m - QPointF(x2, y2))
                yield d, i, (x, y)

    # These two, along with a call to adjustSize are required for the
    # scroll area.
    def sizeHint(self):
        return self.minimumSizeHint()

    def minimumSizeHint(self):
        if self.pixmap:
            return self.scale * self.pixmap.size()
        return super(Canvas, self).minimumSizeHint()

    def wheelEvent(self, ev):
        qt_version = 4 if hasattr(ev, "delta") else 5
        if qt_version == 4:
            if ev.orientation() == Qt.Vertical:
                v_delta = ev.delta()
                h_delta = 0
            else:
                h_delta = ev.delta()
                v_delta = 0
        else:
            delta = ev.angleDelta()
            h_delta = delta.x()
            v_delta = delta.y()

        mods = ev.modifiers()
        if Qt.ControlModifier == int(mods) and v_delta:
            self.zoomRequest.emit(v_delta)
        else:
            v_delta and self.scrollRequest.emit(v_delta, Qt.Vertical)
            h_delta and self.scrollRequest.emit(h_delta, Qt.Horizontal)
        ev.accept()

    def keyPressEvent(self, ev):
        key = ev.key()
        if key == Qt.Key_Escape and self.current:
            print('ESC press')
            self.current = None
            self.drawingPolygon.emit(False)
            self.update()
        elif key == Qt.Key_Return and self.canCloseShape():
            self.finalise()
        elif key == Qt.Key_Left and self.selectedShape:
            self.moveOnePixel('Left')
        elif key == Qt.Key_Right and self.selectedShape:
            self.moveOnePixel('Right')
        elif key == Qt.Key_Up and self.selectedShape:
            self.moveOnePixel('Up')
        elif key == Qt.Key_Down and self.selectedShape:
            self.moveOnePixel('Down')

    def moveOnePixel(self, direction):
        # print(self.selectedShape.points)
        if direction == 'Left' and not self.moveOutOfBound(QPointF(-1.0, 0)):
            # print("move Left one pixel")
            self.selectedShape.points[0] += QPointF(-1.0, 0)
            self.selectedShape.points[1] += QPointF(-1.0, 0)
            self.selectedShape.points[2] += QPointF(-1.0, 0)
            self.selectedShape.points[3] += QPointF(-1.0, 0)
        elif direction == 'Right' and not self.moveOutOfBound(QPointF(1.0, 0)):
            # print("move Right one pixel")
            self.selectedShape.points[0] += QPointF(1.0, 0)
            self.selectedShape.points[1] += QPointF(1.0, 0)
            self.selectedShape.points[2] += QPointF(1.0, 0)
            self.selectedShape.points[3] += QPointF(1.0, 0)
        elif direction == 'Up' and not self.moveOutOfBound(QPointF(0, -1.0)):
            # print("move Up one pixel")
            self.selectedShape.points[0] += QPointF(0, -1.0)
            self.selectedShape.points[1] += QPointF(0, -1.0)
            self.selectedShape.points[2] += QPointF(0, -1.0)
            self.selectedShape.points[3] += QPointF(0, -1.0)
        elif direction == 'Down' and not self.moveOutOfBound(QPointF(0, 1.0)):
            # print("move Down one pixel")
            self.selectedShape.points[0] += QPointF(0, 1.0)
            self.selectedShape.points[1] += QPointF(0, 1.0)
            self.selectedShape.points[2] += QPointF(0, 1.0)
            self.selectedShape.points[3] += QPointF(0, 1.0)
        self.shapeMoved.emit()
        self.repaint()

    def moveOutOfBound(self, step):
        points = [p1+p2 for p1, p2 in zip(self.selectedShape.points, [step]*4)]
        return True in map(self.outOfPixmap, points)

    def setLastLabel(self, text, line_color  = None, fill_color = None):
        assert text
        self.shapes[-1].label = text
        if line_color:
            self.shapes[-1].line_color = line_color
        
        if fill_color:
            self.shapes[-1].fill_color = fill_color

        return self.shapes[-1]

    def undoLastLine(self):
        assert self.shapes
        self.current = self.shapes.pop()
        self.current.setOpen()
        self.line.points = [self.current[-1], self.current[0]]
        self.drawingPolygon.emit(True)

    def resetAllLines(self):
        assert self.shapes
        self.current = self.shapes.pop()
        self.current.setOpen()
        self.line.points = [self.current[-1], self.current[0]]
        self.drawingPolygon.emit(True)
        self.current = None
        self.drawingPolygon.emit(False)
        self.update()

    def loadPixmap(self, pixmap):
        self.pixmap = pixmap
        self.shapes = []
        self.repaint()

    def loadShapes(self, shapes):
        self.shapes = list(shapes)
        self.current = None
        self.repaint()

    def setShapeVisible(self, shape, value):
        self.visible[shape] = value
        self.repaint()

    def currentCursor(self):
        cursor = QApplication.overrideCursor()
        if cursor is not None:
            cursor = cursor.shape()
        return cursor

    def overrideCursor(self, cursor):
        self._cursor = cursor
        if self.currentCursor() is None:
            QApplication.setOverrideCursor(cursor)
        else:
            QApplication.changeOverrideCursor(cursor)

    def restoreCursor(self):
        QApplication.restoreOverrideCursor()

    def resetState(self):
        self.restoreCursor()
        self.pixmap = None
        self.update()
class MainWindow(QMainWindow, WindowMixin):
    FIT_WINDOW, FIT_WIDTH, MANUAL_ZOOM = list(range(3))

    def __init__(self, defaultFilename=None, defaultPrefdefClassFile=None, defaultSaveDir=None):
        super(MainWindow, self).__init__()
        self.setWindowTitle(__appname__)
        try:        
            lsttemplate = next(os.walk(folder))[1]
        except:
            pass
        # Load setting in the main thread
        self.settings = Settings()
        self.settings.load()
        settings = self.settings

        # Save as Pascal voc xml
        self.defaultSaveDir = defaultSaveDir
        self.usingPascalVocFormat = True
        self.usingYoloFormat = False

        # For loading all image under a directory
        self.mImgList = []
        self.dirname = None
        self.labelHist = []
        self.lastOpenDir = None
        self.foder=''


        # Whether we need to save or not.
        self.dirty = False

        self._noSelectionSlot = False
        self._beginner = True
        self.screencastViewer = self.getAvailableScreencastViewer()
        self.screencast = "https://youtu.be/p0nR2YsCY_U"

        # Load predefined classes to the list
        self.loadPredefinedClasses(defaultPrefdefClassFile)

        # Main widgets and related state.
        self.labelDialog = LabelDialog(parent=self, listItem=self.labelHist)

        self.itemsToShapes = {}
        self.shapesToItems = {}
        self.prevLabelText = ''

        listLayout = QVBoxLayout()
        listLayout.setContentsMargins(0, 0, 0, 0)

        # Create a widget for using default label
        self.useDefaultLabelCheckbox = QCheckBox(u'Use default label')
        self.useDefaultLabelCheckbox.setChecked(False)
        self.defaultLabelTextLine = QLineEdit()
        useDefaultLabelQHBoxLayout = QHBoxLayout()
        useDefaultLabelQHBoxLayout.addWidget(self.useDefaultLabelCheckbox)
        useDefaultLabelQHBoxLayout.addWidget(self.defaultLabelTextLine)
        useDefaultLabelContainer = QWidget()
        useDefaultLabelContainer.setLayout(useDefaultLabelQHBoxLayout)

        # Create a widget for edit and diffc button
        #self.diffcButton = QCheckBox(u'difficult')
        #self.diffcButton.setChecked(False)
        #self.diffcButton.stateChanged.connect(self.btnstate)
        self.editButton = QToolButton()
        self.editButton.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)

        # Add combobox
        #self.ComboboxUser = QComboBox()
        #self.ComboboxUser.addItems(lsttemplate)
        #self.ComboboxUser.currentIndexChanged.connect(self.Combobox_select)


        # Add some of widgets to listLayout
        #listLayout.addWidget(self.ComboboxUser)
        listLayout.addWidget(self.editButton)
        #listLayout.addWidget(self.diffcButton)
        listLayout.addWidget(useDefaultLabelContainer)
        #listLayout.addWidget(self.btnok)

        # Create and add a widget for showing current label items
        self.labelList = QListWidget()
        labelListContainer = QWidget()
        labelListContainer.setLayout(listLayout)
        self.labelList.itemActivated.connect(self.labelSelectionChanged)
        self.labelList.itemSelectionChanged.connect(self.labelSelectionChanged)
        self.labelList.itemDoubleClicked.connect(self.editLabel)
        # Connect to itemChanged to detect checkbox changes.
        self.labelList.itemChanged.connect(self.labelItemChanged)
        listLayout.addWidget(self.labelList)

        #self.dock = QDockWidget(u'Box Labels', self)
        self.dock = QDockWidget(u'All Folder', self)
        self.dock.setObjectName(u'Labels')
        self.dock.setWidget(labelListContainer)

        # Tzutalin 20160906 : Add file list and dock to move faster
        self.fileListWidget = QListWidget()
        self.fileListWidget.itemDoubleClicked.connect(self.fileitemDoubleClicked)
        filelistLayout = QVBoxLayout()
        filelistLayout.setContentsMargins(0, 0, 0, 0)
        filelistLayout.addWidget(self.fileListWidget)
        fileListContainer = QWidget()
        fileListContainer.setLayout(filelistLayout)
        self.filedock = QDockWidget(u'File List', self)
        self.filedock.setObjectName(u'Files')
        self.filedock.setWidget(fileListContainer)

        # P Add Dock new
        #self.fileListWidgetcrop = QListWidget()
        #filelistLayoutcrop = QVBoxLayout()
        #filelistLayoutcrop.setContentsMargins(0, 0, 0, 0)
        
        #add button
        self.btnok = QPushButton('OCR', self)
        self.btnok.setMaximumSize(120, 80)
        self.btnok.setStyleSheet('QPushButton {background-color: green; color: white;}')
        self.btnok.clicked.connect(self.buntton_OCR)

        self.pbar = QProgressBar(self)
        self.pbar.setGeometry(0, 100, 100, 30)
        self.timer = QBasicTimer()
        self.step = 0
        


        font = QFont("Sans Serif", 15, QFont.Normal)

        self.editordate = QLabel()
        
        self.editordate.setFont(font)

        self.pbar.reset()
        self.timer.stop()

        listLayout.addWidget(self.btnok)
        listLayout.addWidget(self.pbar)
        listLayout.addWidget(self.editordate)






        self.zoomWidget = ZoomWidget()
        self.colorDialog = ColorDialog(parent=self)

        self.canvas = Canvas(parent=self)
        self.canvas.zoomRequest.connect(self.zoomRequest)

        scroll = QScrollArea()
        scroll.setWidget(self.canvas)
        scroll.setWidgetResizable(True)
        self.scrollBars = {
            Qt.Vertical: scroll.verticalScrollBar(),
            Qt.Horizontal: scroll.horizontalScrollBar()
        }
        self.scrollArea = scroll
        self.canvas.scrollRequest.connect(self.scrollRequest)
        self.canvas.createShape.connect(self.createShape)
        self.canvas.openNextImg.connect(self.openNextImg)
        self.canvas.newShape.connect(self.newShape)
        self.canvas.shapeMoved.connect(self.setDirty)
        self.canvas.selectionChanged.connect(self.shapeSelectionChanged)
        self.canvas.drawingPolygon.connect(self.toggleDrawingSensitive)

        self.setCentralWidget(scroll)
        self.addDockWidget(Qt.RightDockWidgetArea, self.dock)
        # Tzutalin 20160906 : Add file list and dock to move faster
        self.addDockWidget(Qt.RightDockWidgetArea, self.filedock)
        self.filedock.setFeatures(QDockWidget.DockWidgetFloatable)

        self.dockFeatures = QDockWidget.DockWidgetClosable | QDockWidget.DockWidgetFloatable
        self.dock.setFeatures(self.dock.features() ^ self.dockFeatures)
        
               

        # Actions
        action = partial(newAction, self)

        
        quit = action('&Quit', self.close,
                      'Ctrl+Q', 'quit', u'Quit application')

        open = action('&Open', self.openFile,
                      'Ctrl+O', 'open', u'Open image or label file')

        opendir = action('&Open Dir', self.openDirDialog,
                         'Ctrl+u', 'open', u'Open Dir')

        changeSavedir = action('&Change Save Dir', self.changeSavedirDialog,
                               'Ctrl+r', 'open', u'Change default saved Annotation dir')

        openAnnotation = action('&Open Annotation', self.openAnnotationDialog,
                                'Ctrl+Shift+O', 'open', u'Open Annotation')

        openNextImg = action('&Next Image (D)', self.openNextImg,
                             'd', 'next', u'Open Next')

        openPrevImg = action('&Prev Image(A)', self.openPrevImg,
                             'a', 'prev', u'Open Prev')

        verify = action('&Verify Image', self.verifyImg,
                        'space', 'verify', u'Verify Image')

        save = action('&Save', self.saveFile,
                      'Ctrl+S', 'save', u'Save labels to file', enabled=False)

        save_format = action('&PascalVOC', self.change_format,
                      'Ctrl+', 'format_voc', u'Change save format', enabled=True)

        saveAs = action('&Save As', self.saveFileAs,
                        'Ctrl+Shift+S', 'save-as', u'Save labels to a different file', enabled=False)

        close = action('&Close', self.closeFile, 'Ctrl+W', 'close', u'Close current file')

        resetAll = action('&ResetAll', self.resetAll, None, 'resetall', u'Reset all')

        color1 = action('Box Line Color', self.chooseColor1,
                        'Ctrl+L', 'color_line', u'Choose Box line color')

        createMode = action('Create\nRectBox', self.setCreateMode,
                            'w', 'new', u'Start drawing Boxs', enabled=False)
        editMode = action('&Edit\nRectBox', self.setEditMode,
                          'Ctrl+J', 'edit', u'Move and edit Boxs', enabled=False)

        create = action('Create\nRectBox(W)', self.createShape,
                        'w', 'new', u'Draw a new Box', enabled=False)
        outcreate = action('', self.outcreatemode,
                        'ESCAPE',None, '')

        delete = action('Delete\nRectBox(DEL)', self.deleteSelectedShape,
                        'Delete', 'delete', u'Delete', enabled=False)
        copy = action('&Duplicate\nRectBox(Ctrl+D)', self.copySelectedShape,
                      'Ctrl+D', 'copy', u'Create a duplicate of the selected Box',
                      enabled=False)

        advancedMode = action('&Advanced Mode', self.toggleAdvancedMode,
                              'Ctrl+Shift+A', 'expert', u'Switch to advanced mode',
                              checkable=True)

        hideAll = action('&Hide\nRectBox', partial(self.togglePolygons, False),
                         'Ctrl+H', 'hide', u'Hide all Boxs',
                         enabled=False)
        showAll = action('&Show\nRectBox', partial(self.togglePolygons, True),
                         'Ctrl+A', 'hide', u'Show all Boxs',
                         enabled=False)

        help = action('&Tutorial', self.showTutorialDialog, None, 'help', u'Show demos')
        showInfo = action('&Information', self.showInfoDialog, None, 'help', u'Information')

        zoom = QWidgetAction(self)
        zoom.setDefaultWidget(self.zoomWidget)
        self.zoomWidget.setWhatsThis(u"Zoom in or out of the image. Also accessible with"
            " %s and %s from the canvas." % (fmtShortcut("Ctrl+[-+]"),
                                             fmtShortcut("Ctrl+Wheel")))
        self.zoomWidget.setEnabled(False)

        zoomIn = action('Zoom &In', partial(self.addZoom, 10),
                        'Ctrl++', 'zoom-in', u'Increase zoom level', enabled=False)
        zoomOut = action('&Zoom Out', partial(self.addZoom, -10),
                         'Ctrl+-', 'zoom-out', u'Decrease zoom level', enabled=False)
        zoomOrg = action('&Original size', partial(self.setZoom, 100),
                         'Ctrl+=', 'zoom', u'Zoom to original size', enabled=False)
        fitWindow = action('&Fit Window', self.setFitWindow,
                           'Ctrl+F', 'fit-window', u'Zoom follows window size',
                           checkable=True, enabled=False)
        fitWidth = action('Fit &Width', self.setFitWidth,
                          'Ctrl+Shift+F', 'fit-width', u'Zoom follows window width',
                          checkable=True, enabled=False)
        # Group zoom controls into a list for easier toggling.
        zoomActions = (self.zoomWidget, zoomIn, zoomOut,
                       zoomOrg, fitWindow, fitWidth)
        self.zoomMode = self.MANUAL_ZOOM
        self.scalers = {
            self.FIT_WINDOW: self.scaleFitWindow,
            self.FIT_WIDTH: self.scaleFitWidth,
            # Set to one to scale to 100% when loading files.
            self.MANUAL_ZOOM: lambda: 1,
        }

        edit = action('&Edit Label', self.editLabel,
                      'Ctrl+E', 'edit', u'Modify the label of the selected Box',
                      enabled=False)
        self.editButton.setDefaultAction(edit)

        shapeLineColor = action('Shape &Line Color', self.chshapeLineColor,
                                icon='color_line', tip=u'Change the line color for this specific shape',
                                enabled=False)
        shapeFillColor = action('Shape &Fill Color', self.chshapeFillColor,
                                icon='color', tip=u'Change the fill color for this specific shape',
                                enabled=False)

        labels = self.dock.toggleViewAction()
        labels.setText('Show/Hide Label Panel')
        labels.setShortcut('Ctrl+Shift+L')

        # Lavel list context menu.
        labelMenu = QMenu()
        addActions(labelMenu, (edit, delete))
        self.labelList.setContextMenuPolicy(Qt.CustomContextMenu)
        self.labelList.customContextMenuRequested.connect(self.popLabelListMenu)


        # Store actions for further handling.
        self.actions = struct(save=save, save_format=save_format, saveAs=saveAs, open=open, close=close, resetAll = resetAll,
                              lineColor=color1, create=create, outcreate=outcreate, delete=delete, edit=edit, copy=copy,
                              createMode=createMode, editMode=editMode, advancedMode=advancedMode,
                              shapeLineColor=shapeLineColor, shapeFillColor=shapeFillColor,
                              zoom=zoom, zoomIn=zoomIn, zoomOut=zoomOut, zoomOrg=zoomOrg,
                              fitWindow=fitWindow, fitWidth=fitWidth,
                              zoomActions=zoomActions,
                              fileMenuActions=(open, None, save, saveAs, close, resetAll, quit),
                              beginner=(), advanced=(),
                              editMenu=(edit, copy, delete,
                                        None, color1),
                              beginnerContext=(create, edit, copy, delete),
                              advancedContext=(createMode, editMode, edit, copy,
                                               delete, shapeLineColor, shapeFillColor),
                              onLoadActive=(close, create, createMode, editMode),
                              onShapesPresent=(saveAs, hideAll, showAll))

        self.menus = struct(file=self.menu('&File'),
            edit=self.menu('&Edit'),
            view=self.menu('&View'),
            help=self.menu('&Help'),
            recentFiles=QMenu('Open &Recent'),
            labelList=labelMenu)

        # Auto saving : Enable auto saving if pressing next
        self.autoSaving = QAction("Auto Saving", self)
        self.autoSaving.setCheckable(True)
        self.autoSaving.setChecked(settings.get(SETTING_AUTO_SAVE, True))
        # Sync single class mode from PR#106
        self.singleClassMode = QAction("Single Class Mode", self)
        self.singleClassMode.setShortcut("Ctrl+Shift+S")
        self.singleClassMode.setCheckable(True)
        self.singleClassMode.setChecked(settings.get(SETTING_SINGLE_CLASS, False))
        self.lastLabel = None
        # Add option to enable/disable labels being painted at the top of
        # bounding boxes
        self.paintLabelsOption = QAction("Paint Labels", self)
        self.paintLabelsOption.setShortcut("Ctrl+Shift+P")
        self.paintLabelsOption.setCheckable(True)
        self.paintLabelsOption.setChecked(settings.get(SETTING_PAINT_LABEL, False))
        self.paintLabelsOption.triggered.connect(self.togglePaintLabelsOption)

        #addActions(self.menus.file,
                   #(open, opendir, changeSavedir, openAnnotation,
                   #self.menus.recentFiles, save, save_format, saveAs, close,
                   #resetAll, quit))

        addActions(self.menus.file,
                   (None, None, None, openAnnotation, self.menus.recentFiles, None, None, None, close, resetAll, quit))

        addActions(self.menus.help, (help, showInfo))
        addActions(self.menus.view, (self.autoSaving,
            self.singleClassMode,
            self.paintLabelsOption,
            labels, advancedMode, None,
            hideAll, showAll, None,
            zoomIn, zoomOut, zoomOrg, None,
            fitWindow, fitWidth))

        self.menus.file.aboutToShow.connect(self.updateFileMenu)

        # Custom context menu for the canvas widget:
        addActions(self.canvas.menus[0], self.actions.beginnerContext)
        addActions(self.canvas.menus[1], (action('&Copy here', self.copyShape),
            action('&Move here', self.moveShape)))

        self.tools = self.toolbar('Tools')
        #self.actions.beginner = (
            #open, opendir, changeSavedir, openNextImg, openPrevImg, verify,
            #save, save_format, None, create,outcreate, copy, delete, None,
            #zoomIn, zoom, zoomOut, fitWindow, fitWidth)

        self.actions.beginner = (None, None, None, openNextImg, openPrevImg, None, None, None, None, create,outcreate, None, delete, None,
            zoomIn, zoom, zoomOut, fitWindow, fitWidth)

        #self.actions.advanced = (
            #open, opendir, changeSavedir, openNextImg, openPrevImg, save,
            #save_format, None,
            #createMode, editMode, None,
            #hideAll, showAll)

        self.actions.advanced = (None, None, None, openNextImg, openPrevImg, None, None, None,
            createMode, editMode, None,
            hideAll, showAll)

        self.statusBar().showMessage('%s started.' % __appname__)
        self.statusBar().show()

        # Application state.
        self.image = QImage()
        self.filePath = ustr(defaultFilename)
        self.recentFiles = []
        self.maxRecent = 7
        self.lineColor = None
        self.fillColor = None
        self.zoom_level = 100
        self.fit_window = False
        # Add Chris
        self.difficult = False

        ## Fix the compatible issue for qt4 and qt5.  Convert the QStringList
        ## to python list
        if settings.get(SETTING_RECENT_FILES):
            if have_qstring():
                recentFileQStringList = settings.get(SETTING_RECENT_FILES)
                self.recentFiles = [ustr(i) for i in recentFileQStringList]
            else:
                self.recentFiles = recentFileQStringList = settings.get(SETTING_RECENT_FILES)

        size = settings.get(SETTING_WIN_SIZE, QSize(600, 500))
        position = settings.get(SETTING_WIN_POSE, QPoint(0, 0))
        self.resize(size)
        self.move(position)
        saveDir = ustr(settings.get(SETTING_SAVE_DIR, None))
        self.lastOpenDir = ustr(settings.get(SETTING_LAST_OPEN_DIR, None))
        if self.defaultSaveDir is None and saveDir is not None and os.path.exists(saveDir):
            self.defaultSaveDir = saveDir
            self.statusBar().showMessage('%s started. Annotation will be saved to %s' % (__appname__, self.defaultSaveDir))
            self.statusBar().show()

        self.restoreState(settings.get(SETTING_WIN_STATE, QByteArray()))
        Shape.line_color = self.lineColor = QColor(settings.get(SETTING_LINE_COLOR, DEFAULT_LINE_COLOR))
        Shape.fill_color = self.fillColor = QColor(settings.get(SETTING_FILL_COLOR, DEFAULT_FILL_COLOR))
        self.canvas.setDrawingColor(self.lineColor)
        # Add chris
        Shape.difficult = self.difficult

        def xbool(x):
            if isinstance(x, QVariant):
                return x.toBool()
            return bool(x)

        if xbool(settings.get(SETTING_ADVANCE_MODE, False)):
            self.actions.advancedMode.setChecked(True)
            self.toggleAdvancedMode()

        # Populate the File menu dynamically.
        self.updateFileMenu()

        # Since loading the file may take some time, make sure it runs in the
        # background.
        if self.filePath and os.path.isdir(self.filePath):
            self.queueEvent(partial(self.importDirImages, self.filePath or ""))
        elif self.filePath:
            self.queueEvent(partial(self.loadFile, self.filePath or ""))
        
        # Callbacks:
        self.zoomWidget.valueChanged.connect(self.paintCanvas)

        self.populateModeActions()

        # Display cursor coordinates at the right of status bar
        self.labelCoordinates = QLabel('')
        self.statusBar().addPermanentWidget(self.labelCoordinates)

        # Open Dir if deafult file
        if self.filePath and os.path.isdir(self.filePath):
            self.openDirDialog(dirpath=self.filePath)

        try:           
            #self.importDirImages(folder + "/" +
            #self.ComboboxUser.currentText())
            self.importDirImages(folder)
        except:
            pass

    ## Support Functions ##
    def set_format(self, save_format):
        if save_format == FORMAT_PASCALVOC:
            self.actions.save_format.setText(FORMAT_PASCALVOC)
            self.actions.save_format.setIcon(newIcon("format_voc"))
            self.usingPascalVocFormat = True
            self.usingYoloFormat = False
            LabelFile.suffix = XML_EXT

        elif save_format == FORMAT_YOLO:
            self.actions.save_format.setText(FORMAT_YOLO)
            self.actions.save_format.setIcon(newIcon("format_yolo"))
            self.usingPascalVocFormat = False
            self.usingYoloFormat = True
            LabelFile.suffix = TXT_EXT

    def change_format(self):
        if self.usingPascalVocFormat: self.set_format(FORMAT_YOLO)
        elif self.usingYoloFormat: self.set_format(FORMAT_PASCALVOC)

    def noShapes(self):
        return not self.itemsToShapes

    def toggleAdvancedMode(self, value=True):
        self._beginner = not value
        self.canvas.setEditing(True)
        self.populateModeActions()
        self.editButton.setVisible(not value)
        if value:
            self.actions.createMode.setEnabled(True)
            self.actions.editMode.setEnabled(False)
            self.dock.setFeatures(self.dock.features() | self.dockFeatures)
        else:
            self.dock.setFeatures(self.dock.features() ^ self.dockFeatures)

    def populateModeActions(self):
        if self.beginner():
            tool, menu = self.actions.beginner, self.actions.beginnerContext
        else:
            tool, menu = self.actions.advanced, self.actions.advancedContext
        self.tools.clear()
        addActions(self.tools, tool)
        self.canvas.menus[0].clear()
        addActions(self.canvas.menus[0], menu)
        self.menus.edit.clear()
        actions = (self.actions.create,) if self.beginner()\
            else (self.actions.createMode, self.actions.editMode)
        addActions(self.menus.edit, actions + self.actions.editMenu)

    def setBeginner(self):
        self.tools.clear()
        addActions(self.tools, self.actions.beginner)

    def setAdvanced(self):
        self.tools.clear()
        addActions(self.tools, self.actions.advanced)

    def setDirty(self):
        self.dirty = True
        self.actions.save.setEnabled(True)

    def setClean(self):
        self.dirty = False
        self.actions.save.setEnabled(False)
        self.actions.create.setEnabled(True)

    def toggleActions(self, value=True):
        """Enable/Disable widgets which depend on an opened image."""
        for z in self.actions.zoomActions:
            z.setEnabled(value)
        for action in self.actions.onLoadActive:
            action.setEnabled(value)

    def queueEvent(self, function):
        QTimer.singleShot(0, function)

    def status(self, message, delay=5000):
        self.statusBar().showMessage(message, delay)

    def resetState(self):
        self.itemsToShapes.clear()
        self.shapesToItems.clear()
        self.labelList.clear()
        self.filePath = None
        self.imageData = None
        self.labelFile = None
        self.canvas.resetState()
        self.labelCoordinates.clear()

    def currentItem(self):
        items = self.labelList.selectedItems()
        if items:
            return items[0]
        return None

    def addRecentFile(self, filePath):
        if filePath in self.recentFiles:
            self.recentFiles.remove(filePath)
        elif len(self.recentFiles) >= self.maxRecent:
            self.recentFiles.pop()
        self.recentFiles.insert(0, filePath)

    def beginner(self):
        return self._beginner

    def advanced(self):
        return not self.beginner()

    def getAvailableScreencastViewer(self):
        osName = platform.system()

        if osName == 'Windows':
            return ['C:\\Program Files\\Internet Explorer\\iexplore.exe']
        elif osName == 'Linux':
            return ['xdg-open']
        elif osName == 'Darwin':
            return ['open', '-a', 'Safari']

    ## Callbacks ##
    def showTutorialDialog(self):
        subprocess.Popen(self.screencastViewer + [self.screencast])

    def showInfoDialog(self):
        msg = u'Name:{0} \nApp Version:{1} \n{2} '.format(__appname__, __version__, sys.version_info)
        QMessageBox.information(self, u'Information', msg)

    def createShape(self):
        assert self.beginner()
        self.canvas.setEditing(False)
        self.actions.create.setEnabled(False)

    def outcreatemode(self):
        assert self.beginner()
        self.canvas.setEditing(True)
        self.actions.create.setEnabled(True)

    def toggleDrawingSensitive(self, drawing=True):
        """In the middle of drawing, toggling between modes should be disabled."""
        self.actions.editMode.setEnabled(not drawing)
        if not drawing and self.beginner():
            # Cancel creation.
            print('Cancel creation.')
            self.canvas.setEditing(True)
            self.canvas.restoreCursor()
            self.actions.create.setEnabled(True)

    def toggleDrawMode(self, edit=True):
        self.canvas.setEditing(edit)
        self.actions.createMode.setEnabled(edit)
        self.actions.editMode.setEnabled(not edit)

    def setCreateMode(self):
        assert self.advanced()
        self.toggleDrawMode(False)

    def setEditMode(self):
        assert self.advanced()
        self.toggleDrawMode(True)
        self.labelSelectionChanged()

    def updateFileMenu(self):
        currFilePath = self.filePath

        def exists(filename):
            return os.path.exists(filename)
        menu = self.menus.recentFiles
        menu.clear()
        files = [f for f in self.recentFiles if f != currFilePath and exists(f)]
        for i, f in enumerate(files):
            icon = newIcon('labels')
            action = QAction(icon, '&%d %s' % (i + 1, QFileInfo(f).fileName()), self)
            action.triggered.connect(partial(self.loadRecent, f))
            menu.addAction(action)

    def popLabelListMenu(self, point):
        self.menus.labelList.exec_(self.labelList.mapToGlobal(point))

    def editLabel(self):
        if not self.canvas.editing():
            return
        item = self.currentItem()
        text = self.labelDialog.popUp(item.text())
        if text is not None:
            item.setText(text)
            item.setBackground(generateColorByText(text))
            self.setDirty()
            ss
    # Tzutalin 20160906 : Add file list and dock to move faster
    def fileitemDoubleClicked(self, item=None):
        self.pbar.reset()
        self.timer.stop()
        
        currIndex = self.mImgList.index(ustr(item.text()))
        if currIndex < len(self.mImgList):
            filename = self.mImgList[currIndex]
            if filename:
                self.loadFile(filename)

    # Add chris
    def btnstate(self, item=None):
        """ Function to handle difficult examples
        Update on each object """
        if not self.canvas.editing():
            return

        item = self.currentItem()
        if not item: # If not selected Item, take the first one
            item = self.labelList.item(self.labelList.count() - 1)

        #difficult = self.diffcButton.isChecked()

        try:
            shape = self.itemsToShapes[item]
        except:
            pass
        # Checked and Update
        try:
            if difficult != shape.difficult:
                shape.difficult = difficult
                self.setDirty()
            else:  # User probably changed item visibility
                self.canvas.setShapeVisible(shape, item.checkState() == Qt.Checked)
        except:
            pass

    # React to canvas signals.
    def shapeSelectionChanged(self, selected=False):
        if self._noSelectionSlot:
            self._noSelectionSlot = False
        else:
            shape = self.canvas.selectedShape
            if shape:
                self.shapesToItems[shape].setSelected(True)
            else:
                self.labelList.clearSelection()
        self.actions.delete.setEnabled(selected)
        self.actions.copy.setEnabled(selected)
        self.actions.edit.setEnabled(selected)
        self.actions.shapeLineColor.setEnabled(selected)
        self.actions.shapeFillColor.setEnabled(selected)

    def addLabel(self, shape):
        shape.paintLabel = self.paintLabelsOption.isChecked()
        item = HashableQListWidgetItem(shape.label)
        item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
        item.setCheckState(Qt.Checked)
        item.setBackground(generateColorByText(shape.label))
        self.itemsToShapes[item] = shape
        self.shapesToItems[shape] = item
        self.labelList.addItem(item)
        for action in self.actions.onShapesPresent:
            action.setEnabled(True)

    def remLabel(self, shape):
        if shape is None:
            # print('rm empty label')
            return
        item = self.shapesToItems[shape]
        self.labelList.takeItem(self.labelList.row(item))
        del self.shapesToItems[shape]
        del self.itemsToShapes[item]

    def loadLabels(self, shapes):
        s = []
        for label, points, line_color, fill_color, difficult in shapes:
            shape = Shape(label=label)
            for x, y in points:
                shape.addPoint(QPointF(x, y))
            shape.difficult = difficult
            shape.close()
            s.append(shape)

            if line_color:
                shape.line_color = QColor(*line_color)
            else:
                shape.line_color = generateColorByText(label)

            if fill_color:
                shape.fill_color = QColor(*fill_color)
            else:
                shape.fill_color = generateColorByText(label)

            self.addLabel(shape)

        self.canvas.loadShapes(s)

    def saveLabels(self, annotationFilePath):
        annotationFilePath = ustr(annotationFilePath)
        if self.labelFile is None:
            self.labelFile = LabelFile()
            self.labelFile.verified = self.canvas.verified

        def format_shape(s):
            return dict(label=s.label,
                        line_color=s.line_color.getRgb(),
                        fill_color=s.fill_color.getRgb(),
                        points=[(p.x(), p.y()) for p in s.points],
                       # add chris
                        difficult = s.difficult)

        shapes = [format_shape(shape) for shape in self.canvas.shapes]
        # Can add differrent annotation formats here
        try:
            if self.usingPascalVocFormat is True:
                if ustr(annotationFilePath[-4:]) != ".xml":
                    annotationFilePath += XML_EXT
                print('Img: ' + self.filePath + ' -> Its xml: ' + annotationFilePath)
                self.labelFile.savePascalVocFormat(annotationFilePath, shapes,self.filePath, self.imageData,
                                                   self.lineColor.getRgb(), self.fillColor.getRgb())
            elif self.usingYoloFormat is True:
                if annotationFilePath[-4:] != ".txt":
                    annotationFilePath += TXT_EXT
                print('Img: ' + self.filePath + ' -> Its txt: ' + annotationFilePath)
                self.labelFile.saveYoloFormat(annotationFilePath, shapes, self.filePath, self.imageData, self.labelHist,
                                                   self.lineColor.getRgb(), self.fillColor.getRgb())
            else:
                self.labelFile.save(annotationFilePath, shapes, self.filePath, self.imageData,
                                    self.lineColor.getRgb(), self.fillColor.getRgb())
            return True
        except LabelFileError as e:
            self.errorMessage(u'Error saving label data', u'<b>%s</b>' % e)
            return False

    def Combobox_select(self):
        #self.importDirImages(folder + "/" + self.ComboboxUser.currentText())
        self.importDirImages(folder)  

    def timerEvent(self, e):
        global solabel
        global foldermodel
        #if self.step >= self.labelList.count():
            
        #    self.timer.stop()
        #    self.step = 0
        #    QMessageBox.about(self, "Info", "Complete")
        #    return
        tVocParseReader = PascalVocReader(xml)
        shapes = tVocParseReader.getShapes()
        nn = np.fromfile(imagenow, np.uint8)
        imggoc = cv2.imdecode(nn, cv2.IMREAD_COLOR)
        typeocr = "Date"

        strdate = 'Date:\n'
        strkana = 'Kana:\n'
        strwh = 'Write_Hand:\n'
        strdatemonshin = 'Date_monshin:\n'
        strcode = 'Code:\n'
        strphone = 'Phone_Number:\n'
        strtracnghiem = 'True_false:\n'
        strocr = 'OCR:\n'
        strocr_vt = 'OCR_WH:\n'

        strname = ''
        #setplant
        for i in range(solabel):
            self.step = self.step + 1
            self.pbar.setValue(self.step)
            typeocr = str(shapes[i][0])
            if typeocr == "Date_Kenshin":
                #self.editordate.setText('Datetime:\n' +return_date(imggoc,str(shapes[i][1]))+'\n--------')
                strdate = strdate + return_date(imggoc,str(shapes[i][1])) + '\n'
            elif typeocr == "Kana":
                #self.editorkana.setText('Kana:\n' + return_kana(imggoc,str(shapes[i][1]))+'\n--------')
                strkana = strkana + return_kana(imggoc,str(shapes[i][1])) + '\n'
            elif typeocr == "Writehand_Number":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                strwh = strwh + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.') + '\n'
            elif typeocr == "Date_Monshin":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                strdatemonshin = strdatemonshin + return_date_monshin(imggoc,str(shapes[i][1])) + '\n'
            elif typeocr == "Code_Number":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                strcode = strcode + return_code(imggoc,str(shapes[i][1])) + '\n'
            elif typeocr == "Phone_Number":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                strphone = strphone + return_sdt(imggoc,str(shapes[i][1])) + '\n'
            elif typeocr == "True_False":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                strtracnghiem = strtracnghiem + return_truefalse(imggoc,str(shapes[i][1])) + '\n'
            elif typeocr == "OCR":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                try:
                    strocr = strocr + OCR_Google_new(imggoc,str(shapes[i][1])) + '\n'
                except:
                    QMessageBox.about(self, "Warning", "Template: OCR cannot connect to VBPO Cloud")
            elif typeocr == "OCR_WH":
                #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
                try:
                    strocr_vt = strocr_vt + OCR_Google_WH(imggoc,str(shapes[i][1])) + '\n'
                except:
                    QMessageBox.about(self, "Warning", "Template: OCR cannot connect to VBPO Cloud")
        if strdate != 'Date:\n':
            strname = strname + strdate + '\n'
        else:
            strname = strname + '\n'
        if strkana != 'Kana:\n':
            strname = strname + strkana + '\n'
        else:
            strname = strname + '\n'
        if strwh != 'Write_Hand:\n':
            strname = strname + strwh + '\n'
        else:
            strname = strname + '\n'
        if strdatemonshin != 'Date_monshin:\n':
            strname = strname + strdatemonshin + '\n'
        else:
            strname = strname + '\n'
        if strcode != 'Code:\n':
            strname = strname + strcode + '\n'
        else:
            strname = strname + '\n'
        if strphone != 'Phone_Number:\n':
            strname = strname + strphone + '\n'
        else:
            strname = strname + '\n'
        if strtracnghiem != 'True_false:\n':
            strname = strname + strtracnghiem + '\n'
        else:
            strname = strname + '\n'
        if strocr != 'OCR:\n':
            strname = strname + strocr + '\n'
        else:
            strname = strname + '\n'
        if strocr_vt != 'OCR_WH:\n':
            strname = strname + strocr_vt + '\n'
        else:
            strname = strname + '\n'
        self.editordate.setText(strname)
        with open(imagenow.split('.')[0] + '.txt', "w", encoding='utf-8') as text_file:
            text_file.write(strname)     
               
        self.timer.stop()
        self.step = 0
        QMessageBox.about(self, "Info", "Complete")


    def buntton_OCR(self):
        global imagenow
        global xml
        global solabel
        global foldermodel
        foldermodel = os.getcwd() + '\model'
        #đ
        self.pbar.reset()
        self.saveFile()
        solabel = self.labelList.count()
        self.pbar.setMaximum(solabel)
        self.timer.start(solabel, self)
        #if solabel > 3:
        #    QMessageBox.about(self, "Warning", "Not Enough Label ( Must be Three )")
        #else:
            #svt date kana
            #xmlPath = os.path.join(self.defaultSaveDir, allname + XML_EXT)


        #tVocParseReader = PascalVocReader(xml)
        #shapes = tVocParseReader.getShapes()
        #nn = np.fromfile(imagenow, np.uint8)
        #imggoc = cv2.imdecode(nn, cv2.IMREAD_COLOR)
        #typeocr = "Date"

        #strdate = 'Date:\n'
        #strkana = 'Kana:\n'
        #strwh = 'Write_Hand:\n'
        #strdatemonshin = 'Date_monshin:\n'
        #strcode = 'Code:\n'
        #strphone = 'Phone_Number:\n'
        #strtracnghiem = 'True_false:\n'
        #strocr = 'OCR:\n'

        #strname = ''
        ##setplant
        #for i in range(solabel):
        #    typeocr = str(shapes[i][0])
        #    if typeocr == "Date_Kenshin":
        #        #self.editordate.setText('Datetime:\n' +return_date(imggoc,str(shapes[i][1]))+'\n--------')
        #        strdate = strdate + return_date(imggoc,str(shapes[i][1])) + '\n'
        #    elif typeocr == "Kana":
        #        #self.editorkana.setText('Kana:\n' + return_kana(imggoc,str(shapes[i][1]))+'\n--------')
        #        strkana = strkana + return_kana(imggoc,str(shapes[i][1])) + '\n'
        #    elif typeocr == "Writehand_Number":
        #        #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
        #        strwh = strwh + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.') + '\n'
        #    elif typeocr == "Date_Monshin":
        #        #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
        #        strdatemonshin = strdatemonshin + return_date_monshin(imggoc,str(shapes[i][1])) + '\n'
        #    elif typeocr == "Code_Number":
        #        #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
        #        strcode = strcode + return_code(imggoc,str(shapes[i][1])) + '\n'
        #    elif typeocr == "Phone_Number":
        #        #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
        #        strphone = strphone + return_sdt(imggoc,str(shapes[i][1])) + '\n'
        #    elif typeocr == "True_False":
        #        #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
        #        strtracnghiem = strtracnghiem + return_truefalse(imggoc,str(shapes[i][1])) + '\n'
        #    elif typeocr == "OCR":
        #        #self.editornumber.setText('Number:\n' + return_number(imggoc,str(shapes[i][1])).replace('@','').replace('$','.')+'\n--------')
        #        try:
        #            strocr = strocr + Return_OCR_Google(imggoc,str(shapes[i][1])) + '\n'
        #        except:
        #            QMessageBox.about(self, "Warning", "Template: OCR cannot connect to VBPO Cloud")
            
        #if strdate != 'Date:\n':
        #    strname = strname + strdate + '\n'
        #if strkana != 'Kana:\n':
        #    strname = strname + strkana + '\n'
        #if strwh != 'Write_Hand:\n':
        #    strname = strname + strwh + '\n'
        #if strdatemonshin != 'Date_monshin:\n':
        #    strname = strname + strdatemonshin + '\n'
        #if strcode != 'Code:\n':
        #    strname = strname + strcode + '\n'
        #if strphone != 'Phone_Number:\n':
        #    strname = strname + strphone + '\n'
        #if strtracnghiem != 'True_false:\n':
        #    strname = strname + strtracnghiem + '\n'
        #if strocr != 'OCR:\n':
        #    strname = strname + strocr + '\n'

        #self.editordate.setText(strname)
        #with open(imagenow.split('.')[0] + '.txt', "w", encoding='utf-8') as text_file:
        #    text_file.write(strname)
        #QMessageBox.about(self, "Info", "Complete")


            #cv2.imshow("",imgwh)
            #cv2.waitKey(0)


    def copySelectedShape(self):
        self.addLabel(self.canvas.copySelectedShape())
        # fix copy and delete
        self.shapeSelectionChanged(True)

    def labelSelectionChanged(self):
        item = self.currentItem()
        if item and self.canvas.editing():
            self._noSelectionSlot = True
            self.canvas.selectShape(self.itemsToShapes[item])
            shape = self.itemsToShapes[item]
            # Add Chris
            #self.diffcButton.setChecked(shape.difficult)

    def labelItemChanged(self, item):
        shape = self.itemsToShapes[item]
        label = item.text()
        if label != shape.label:
            shape.label = item.text()
            shape.line_color = generateColorByText(shape.label)
            self.setDirty()
        else:  # User probably changed item visibility
            self.canvas.setShapeVisible(shape, item.checkState() == Qt.Checked)

    # Callback functions:
    def newShape(self):
        """Pop-up and give focus to the label editor.

        position MUST be in global coordinates.
        """
        if not self.useDefaultLabelCheckbox.isChecked() or not self.defaultLabelTextLine.text():
            if len(self.labelHist) > 0:
                self.labelDialog = LabelDialog(parent=self, listItem=self.labelHist)

            # Sync single class mode from PR#106
            if self.singleClassMode.isChecked() and self.lastLabel:
                text = self.lastLabel
            else:
                text = self.labelDialog.popUp(text=self.prevLabelText)
                self.lastLabel = text
        else:
            text = self.defaultLabelTextLine.text()

        # Add Chris
        #self.diffcButton.setChecked(False)
        if text is not None:
            self.prevLabelText = text
            generate_color = generateColorByText(text)
            shape = self.canvas.setLastLabel(text, generate_color, generate_color)
            self.addLabel(shape)
            if self.beginner():  # Switch to edit mode.
                self.canvas.setEditing(True)
                self.actions.create.setEnabled(True)
            else:
                self.actions.editMode.setEnabled(True)
            self.setDirty()

            if text not in self.labelHist:
                self.labelHist.append(text)
        else:
            # self.canvas.undoLastLine()
            self.canvas.resetAllLines()

    def scrollRequest(self, delta, orientation):
        units = - delta / (8 * 15)
        bar = self.scrollBars[orientation]
        bar.setValue(bar.value() + bar.singleStep() * units)

    def setZoom(self, value):
        self.actions.fitWidth.setChecked(False)
        self.actions.fitWindow.setChecked(False)
        self.zoomMode = self.MANUAL_ZOOM
        self.zoomWidget.setValue(value)

    def addZoom(self, increment=10):
        self.setZoom(self.zoomWidget.value() + increment)

    def zoomRequest(self, delta):
        # get the current scrollbar positions
        # calculate the percentages ~ coordinates
        h_bar = self.scrollBars[Qt.Horizontal]
        v_bar = self.scrollBars[Qt.Vertical]

        # get the current maximum, to know the difference after zooming
        h_bar_max = h_bar.maximum()
        v_bar_max = v_bar.maximum()

        # get the cursor position and canvas size
        # calculate the desired movement from 0 to 1
        # where 0 = move left
        #       1 = move right
        # up and down analogous
        cursor = QCursor()
        pos = cursor.pos()
        relative_pos = QWidget.mapFromGlobal(self, pos)

        cursor_x = relative_pos.x()
        cursor_y = relative_pos.y()

        w = self.scrollArea.width()
        h = self.scrollArea.height()

        # the scaling from 0 to 1 has some padding
        # you don't have to hit the very leftmost pixel for a maximum-left
        # movement
        margin = 0.1
        move_x = (cursor_x - margin * w) / (w - 2 * margin * w)
        move_y = (cursor_y - margin * h) / (h - 2 * margin * h)

        # clamp the values from 0 to 1
        move_x = min(max(move_x, 0), 1)
        move_y = min(max(move_y, 0), 1)

        # zoom in
        units = delta / (8 * 15)
        scale = 10
        self.addZoom(scale * units)

        # get the difference in scrollbar values
        # this is how far we can move
        d_h_bar_max = h_bar.maximum() - h_bar_max
        d_v_bar_max = v_bar.maximum() - v_bar_max

        # get the new scrollbar values
        new_h_bar_value = h_bar.value() + move_x * d_h_bar_max
        new_v_bar_value = v_bar.value() + move_y * d_v_bar_max

        h_bar.setValue(new_h_bar_value)
        v_bar.setValue(new_v_bar_value)

    def setFitWindow(self, value=True):
        if value:
            self.actions.fitWidth.setChecked(False)
        self.zoomMode = self.FIT_WINDOW if value else self.MANUAL_ZOOM
        self.adjustScale()

    def setFitWidth(self, value=True):
        if value:
            self.actions.fitWindow.setChecked(False)
        self.zoomMode = self.FIT_WIDTH if value else self.MANUAL_ZOOM
        self.adjustScale()

    def togglePolygons(self, value):
        for item, shape in self.itemsToShapes.items():
            item.setCheckState(Qt.Checked if value else Qt.Unchecked)

    def loadFile(self, filePath=None):
        global giatridau
        global allname
        global imagenow
        global xml
        """Load the specified file, or the last opened file if None."""
        self.resetState()
        self.canvas.setEnabled(False)
        if filePath is None:
            filePath = self.settings.get(SETTING_FILENAME)

        # Make sure that filePath is a regular python string, rather than
        # QString
        filePath = ustr(filePath)

        unicodeFilePath = ustr(filePath)
        imagenow = unicodeFilePath
        # Tzutalin 20160906 : Add file list and dock to move faster
        # Highlight the file item
        if unicodeFilePath and self.fileListWidget.count() > 0:
            index = self.mImgList.index(unicodeFilePath)
            fileWidgetItem = self.fileListWidget.item(index)
            fileWidgetItem.setSelected(True)

        if unicodeFilePath and os.path.exists(unicodeFilePath):
            if LabelFile.isLabelFile(unicodeFilePath):
                try:
                    self.labelFile = LabelFile(unicodeFilePath)
                except LabelFileError as e:
                    self.errorMessage(u'Error opening file',
                                      (u"<p><b>%s</b></p>"
                                       u"<p>Make sure <i>%s</i> is a valid label file.") % (e, unicodeFilePath))
                    self.status("Error reading %s" % unicodeFilePath)
                    return False
                self.imageData = self.labelFile.imageData
                self.lineColor = QColor(*self.labelFile.lineColor)
                self.fillColor = QColor(*self.labelFile.fillColor)
                self.canvas.verified = self.labelFile.verified
            else:
                # Load image:
                # read data first and store for saving into label file.
                self.imageData = read(unicodeFilePath, None)
                self.labelFile = None
                self.canvas.verified = False

            image = QImage.fromData(self.imageData)
            if image.isNull():
                self.errorMessage(u'Error opening file',
                                  u"<p>Make sure <i>%s</i> is a valid image file." % unicodeFilePath)
                self.status("Error reading %s" % unicodeFilePath)
                return False
            self.status("Loaded %s" % os.path.basename(unicodeFilePath))
            self.image = image
            self.filePath = unicodeFilePath
            self.canvas.loadPixmap(QPixmap.fromImage(image))
            if self.labelFile:
                self.loadLabels(self.labelFile.shapes)
            self.setClean()
            self.canvas.setEnabled(True)
            self.adjustScale(initial=True)
            self.paintCanvas()
            self.addRecentFile(self.filePath)
            self.toggleActions(True)

            # Label xml file and show bound box according to its filename
            # if self.usingPascalVocFormat is True:
            if self.defaultSaveDir is not None:      
                basename = os.path.basename(os.path.splitext(self.filePath)[0])          
                if giatridau == 0:                    
                    allname = basename
                    giatridau = 1
                xmlPathdau = os.path.join(self.defaultSaveDir, allname + XML_EXT)
                xmlPath = os.path.join(self.defaultSaveDir, basename + XML_EXT)
                txtPath = os.path.join(self.defaultSaveDir, basename + TXT_EXT)
                xml = xmlPath
                """Annotation file priority:
                PascalXML > YOLO
                """
                if os.path.isfile(xmlPath):
                    self.loadPascalXMLByFilename(xmlPath)
                else:
                    self.loadPascalXMLByFilename(xmlPathdau)
                #elif os.path.isfile(txtPath):
                #    self.loadYOLOTXTByFilename(txtPath)
            else:
                xmlPath = os.path.splitext(filePath)[0] + XML_EXT
                txtPath = os.path.splitext(filePath)[0] + TXT_EXT
                if os.path.isfile(xmlPath):
                    self.loadPascalXMLByFilename(xmlPath)
                else:
                    self.loadPascalXMLByFilename(xmlPathdau)
                #elif os.path.isfile(txtPath):
                #    self.loadYOLOTXTByFilename(txtPath)

            self.setWindowTitle(__appname__ + ' ' + filePath)

            # Default : select last item if there is at least one item
            if self.labelList.count():
                self.labelList.setCurrentItem(self.labelList.item(self.labelList.count() - 1))
                self.labelList.item(self.labelList.count() - 1).setSelected(True)

            self.canvas.setFocus(True)
            return True
        return False

    def resizeEvent(self, event):
        if self.canvas and not self.image.isNull()\
           and self.zoomMode != self.MANUAL_ZOOM:
            self.adjustScale()
        super(MainWindow, self).resizeEvent(event)

    def paintCanvas(self):
        assert not self.image.isNull(), "cannot paint null image"
        self.canvas.scale = 0.01 * self.zoomWidget.value()
        self.canvas.adjustSize()
        self.canvas.update()

    def adjustScale(self, initial=False):
        value = self.scalers[self.FIT_WINDOW if initial else self.zoomMode]()
        self.zoomWidget.setValue(int(100 * value))

    def scaleFitWindow(self):
        """Figure out the size of the pixmap in order to fit the main widget."""
        e = 2.0  # So that no scrollbars are generated.
        w1 = self.centralWidget().width() - e
        h1 = self.centralWidget().height() - e
        a1 = w1 / h1
        # Calculate a new scale value based on the pixmap's aspect ratio.
        w2 = self.canvas.pixmap.width() - 0.0
        h2 = self.canvas.pixmap.height() - 0.0
        a2 = w2 / h2
        return w1 / w2 if a2 >= a1 else h1 / h2

    def scaleFitWidth(self):
        # The epsilon does not seem to work too well here.
        w = self.centralWidget().width() - 2.0
        return w / self.canvas.pixmap.width()

    def closeEvent(self, event):
        if not self.mayContinue():
            event.ignore()
        settings = self.settings
        # If it loads images from dir, don't load it at the begining
        if self.dirname is None:
            settings[SETTING_FILENAME] = self.filePath if self.filePath else ''
        else:
            settings[SETTING_FILENAME] = ''

        settings[SETTING_WIN_SIZE] = self.size()
        settings[SETTING_WIN_POSE] = self.pos()
        settings[SETTING_WIN_STATE] = self.saveState()
        settings[SETTING_LINE_COLOR] = self.lineColor
        settings[SETTING_FILL_COLOR] = self.fillColor
        settings[SETTING_RECENT_FILES] = self.recentFiles
        settings[SETTING_ADVANCE_MODE] = not self._beginner
        if self.defaultSaveDir and os.path.exists(self.defaultSaveDir):
            settings[SETTING_SAVE_DIR] = ustr(self.defaultSaveDir)
        else:
            settings[SETTING_SAVE_DIR] = ""

        if self.lastOpenDir and os.path.exists(self.lastOpenDir):
            settings[SETTING_LAST_OPEN_DIR] = self.lastOpenDir
        else:
            settings[SETTING_LAST_OPEN_DIR] = ""

        settings[SETTING_AUTO_SAVE] = self.autoSaving.isChecked()
        settings[SETTING_SINGLE_CLASS] = self.singleClassMode.isChecked()
        settings[SETTING_PAINT_LABEL] = self.paintLabelsOption.isChecked()
        settings.save()
    ## User Dialogs ##

    def loadRecent(self, filename):
        if self.mayContinue():
            self.loadFile(filename)

    def scanAllImages(self, folderPath):
        #extensions = ['.%s' % fmt.data().decode("ascii").lower() for fmt in
        #QImageReader.supportedImageFormats()]
        images = []
        imgcheck = []
        for root, dirs, files in os.walk(folderPath):            
            if not os.path.exists(folderPath + "/" + username):
                os.makedirs(folderPath + "/" + username) 
            try:
                imgcheck = [f for f in os.listdir(folderPath + "/" + username) if f.endswith(".tif") or f.endswith(".jpg")]
                for i in range(len(imgcheck)):
                    imgcheck[i] = folderPath + "/" + username + "/" + imgcheck[i]
            except:
                pass   
            for file in files:
                #if file.lower().endswith(tuple(extensions)):
                if file.lower().endswith(".pdf"):
                    #relativePath = os.path.join(root, file)
                    relativePath = root + "/" + file
                    folderpdf = root + '/' + file.replace(".pdf","")                                       
                    uptolist = 0
                     
                    if len(imgcheck) == 0:    
                        images = convertPDF2JPG(file, root ,file.replace(".pdf",""))
                        for j in range(len(images)):
                            imgcheck.append(folderPath + "/" + username + "/" + images[j])
                    else:
                        for j in range(len(imgcheck)):
                            checkimage = imgcheck[j]
                            if file.replace(".pdf","").split('/')[len(file.replace(".pdf","").split('/')) - 1] + "-" in checkimage:
                                uptolist = 1
                                j = len(imgcheck)
                        if uptolist == 0:
                            images = convertPDF2JPG(file, root ,file.replace(".pdf",""))
                            for j in range(len(images)):
                                imgcheck.append(folderPath + "/" + username + "/" + images[j])
                    #path = ustr(os.path.abspath(relativePath))
                    #images.append(path)
        imgcheck.sort(key=lambda x: x.lower())
        return imgcheck

    def changeSavedirDialog(self, _value=False):
        if self.defaultSaveDir is not None:
            path = ustr(self.defaultSaveDir)
        else:
            path = '.'

        dirpath = ustr(QFileDialog.getExistingDirectory(self,
                                                       '%s - Save annotations to the directory' % __appname__, path,  QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks))

        if dirpath is not None and len(dirpath) > 1:
            self.defaultSaveDir = dirpath

        self.statusBar().showMessage('%s . Annotation will be saved to %s' % ('Change saved folder', self.defaultSaveDir))
        self.statusBar().show()

    def openAnnotationDialog(self, _value=False):
        global folder
        folder = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        
        try:        
            lsttemplate = next(os.walk(folder))[1]
        except:
            pass
        self.importDirImages(folder)


    def openAnnotationDialog2(self, _value=False):
        if self.filePath is None:
            self.statusBar().showMessage('Please select image first')
            self.statusBar().show()
            return

        path = os.path.dirname(ustr(self.filePath))\
            if self.filePath else '.'
        if self.usingPascalVocFormat:
            filters = "Open Annotation XML file (%s)" % ' '.join(['*.xml'])
            filename = ustr(QFileDialog.getOpenFileName(self,'%s - Choose a xml file' % __appname__, path, filters))
            if filename:
                if isinstance(filename, (tuple, list)):
                    filename = filename[0]
            self.loadPascalXMLByFilename(filename)

    def openDirDialog(self, _value=False, dirpath=None):
        if not self.mayContinue():
            return

        defaultOpenDirPath = dirpath if dirpath else '.'
        if self.lastOpenDir and os.path.exists(self.lastOpenDir):
            defaultOpenDirPath = self.lastOpenDir
        else:
            defaultOpenDirPath = os.path.dirname(self.filePath) if self.filePath else '.'

        targetDirPath = ustr(QFileDialog.getExistingDirectory(self,
                                                     '%s - Open Directory' % __appname__, defaultOpenDirPath,
                                                     QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks))
        self.importDirImages(targetDirPath)

    def importDirImages(self, dirpath):
        if not self.mayContinue() or not dirpath:
            return

        if not os.path.exists(dirpath + "/" + username + '-Result-xml'):
            os.makedirs(dirpath + "/" + username + '-Result-xml')

        self.defaultSaveDir = dirpath + "/" + username + '-Result-xml'
        self.foder=dirpath.split('/')[-1]
        self.lastOpenDir = dirpath + "/" + username
        self.dirname = dirpath + "/" + username
        self.filePath = None
        self.fileListWidget.clear()
        self.mImgList = self.scanAllImages(dirpath)
        self.openNextImg()



        for imgPath in self.mImgList:
            item = QListWidgetItem(imgPath)
            self.fileListWidget.addItem(item)

    def verifyImg(self, _value=False):
        # Proceding next image without dialog if having any label
         if self.filePath is not None:
            try:
                self.labelFile.toggleVerify()
            except AttributeError:
                # If the labelling file does not exist yet, create if and
                # re-save it with the verified attribute.
                self.saveFile()
                if self.labelFile != None:
                    self.labelFile.toggleVerify()
                else:
                    return

            self.canvas.verified = self.labelFile.verified
            self.paintCanvas()
            self.saveFile()

    def openPrevImg(self, _value=False):
        # Proceding prev image without dialog if having any label
        self.pbar.reset()
        self.timer.stop()
        
        #self.editornumber.setText('\n')
        #self.editorkana.setText('\n')
        if self.autoSaving.isChecked():
            if self.defaultSaveDir is not None:
                if self.dirty is True:
                    self.saveFile()
                else:
                    self.saveFile()
            else:
                self.changeSavedirDialog()
                return

        if not self.mayContinue():
            return

        if len(self.mImgList) <= 0:
            return

        if self.filePath is None:
            return

        currIndex = self.mImgList.index(self.filePath)
        if currIndex - 1 >= 0:
            filename = self.mImgList[currIndex - 1]
            if filename:
                self.loadFile(filename)

    def openNextImg(self, _value=False):
        # Proceding prev image without dialog if having any label
        self.pbar.reset()
        self.timer.stop()

        self.autoSaving.setChecked(True)
        if self.autoSaving.isChecked():
            if self.defaultSaveDir is not None:
                if self.dirty is True:
                    self.saveFile()
                else:
                    self.saveFile()
            else:
                self.changeSavedirDialog()
                return

        if not self.mayContinue():
            return

        if len(self.mImgList) <= 0:
            return

        filename = None
        if self.filePath is None:
            filename = self.mImgList[0]
        else:
            currIndex = self.mImgList.index(self.filePath)
            if currIndex + 1 < len(self.mImgList):
                filename = self.mImgList[currIndex + 1]

        if filename:
            self.loadFile(filename)
            self.createShape()

    def openFile(self, _value=False):
        if not self.mayContinue():
            return
        path = os.path.dirname(ustr(self.filePath)) if self.filePath else '.'
        formats = ['*.%s' % fmt.data().decode("ascii").lower() for fmt in QImageReader.supportedImageFormats()]
        filters = "Image & Label files (%s)" % ' '.join(formats + ['*%s' % LabelFile.suffix])
        filename = QFileDialog.getOpenFileName(self, '%s - Choose Image or Label file' % __appname__, path, filters)
        if filename:
            if isinstance(filename, (tuple, list)):
                filename = filename[0]
            self.loadFile(filename)

    def saveFile(self, _value=False):
        if self.defaultSaveDir is not None and len(ustr(self.defaultSaveDir)):
            if self.filePath:
                imgFileName = os.path.basename(self.filePath)
                savedFileName = os.path.splitext(imgFileName)[0]
                
                savedPath = os.path.join(ustr(self.defaultSaveDir),savedFileName)
                self._saveFile(savedPath)
        else:
            imgFileDir = os.path.dirname(self.filePath)
            imgFileName = os.path.basename(self.filePath)
            savedFileName = os.path.splitext(imgFileName)[0]
            savedPath = os.path.join(imgFileDir, savedFileName)
            self._saveFile(savedPath if self.labelFile
                           else self.saveFileDialog())

    def saveFileAs(self, _value=False):
        assert not self.image.isNull(), "cannot save empty image"
        self._saveFile(self.saveFileDialog())

    def saveFileDialog(self):
        caption = '%s - Choose File' % __appname__
        filters = 'File (*%s)' % LabelFile.suffix
        openDialogPath = self.currentPath()
        dlg = QFileDialog(self, caption, openDialogPath, filters)
        dlg.setDefaultSuffix(LabelFile.suffix[1:])
        dlg.setAcceptMode(QFileDialog.AcceptSave)
        filenameWithoutExtension = os.path.splitext(self.filePath)[0]
        dlg.selectFile(filenameWithoutExtension)
        dlg.setOption(QFileDialog.DontUseNativeDialog, False)
        if dlg.exec_():
            fullFilePath = ustr(dlg.selectedFiles()[0])
            return os.path.splitext(fullFilePath)[0] # Return file path without the extension.
        return ''

    def _saveFile(self, annotationFilePath):
        if annotationFilePath and self.saveLabels(annotationFilePath):
            self.setClean()
            self.statusBar().showMessage('Saved to  %s' % annotationFilePath)
            self.statusBar().show()

    def closeFile(self, _value=False):
        if not self.mayContinue():
            return
        self.resetState()
        self.setClean()
        self.toggleActions(False)
        self.canvas.setEnabled(False)
        self.actions.saveAs.setEnabled(False)

    def resetAll(self):
        self.settings.reset()
        self.close()
        proc = QProcess()
        proc.startDetached(os.path.abspath(__file__))

    def mayContinue(self):
        return not (self.dirty and not self.discardChangesDialog())

    def discardChangesDialog(self):
        yes, no = QMessageBox.Yes, QMessageBox.No
        msg = u'You have unsaved changes, proceed anyway?'
        return yes == QMessageBox.warning(self, u'Attention', msg, yes | no)

    def errorMessage(self, title, message):
        return QMessageBox.critical(self, title,
                                    '<p><b>%s</b></p>%s' % (title, message))

    def currentPath(self):
        return os.path.dirname(self.filePath) if self.filePath else '.'

    def chooseColor1(self):
        color = self.colorDialog.getColor(self.lineColor, u'Choose line color',
                                          default=DEFAULT_LINE_COLOR)
        if color:
            self.lineColor = color
            Shape.line_color = color
            self.canvas.setDrawingColor(color)
            self.canvas.update()
            self.setDirty()

    def deleteSelectedShape(self):
        self.remLabel(self.canvas.deleteSelected())
        self.setDirty()
        if self.noShapes():
            for action in self.actions.onShapesPresent:
                action.setEnabled(False)

    def chshapeLineColor(self):
        color = self.colorDialog.getColor(self.lineColor, u'Choose line color',
                                          default=DEFAULT_LINE_COLOR)
        if color:
            self.canvas.selectedShape.line_color = color
            self.canvas.update()
            self.setDirty()

    def chshapeFillColor(self):
        color = self.colorDialog.getColor(self.fillColor, u'Choose fill color',
                                          default=DEFAULT_FILL_COLOR)
        if color:
            self.canvas.selectedShape.fill_color = color
            self.canvas.update()
            self.setDirty()

    def copyShape(self):
        self.canvas.endMove(copy=True)
        self.addLabel(self.canvas.selectedShape)
        self.setDirty()

    def moveShape(self):
        self.canvas.endMove(copy=False)
        self.setDirty()

    def loadPredefinedClasses(self, predefClassesFile):
        if os.path.exists(predefClassesFile) is True:
            with codecs.open(predefClassesFile, 'r', 'utf8') as f:
                for line in f:
                    line = line.strip()
                    if self.labelHist is None:
                        self.labelHist = [line]
                    else:
                        self.labelHist.append(line)

    def loadPascalXMLByFilename(self, xmlPath):
        if self.filePath is None:
            return
        if os.path.isfile(xmlPath) is False:
            return

        self.set_format(FORMAT_PASCALVOC)

        tVocParseReader = PascalVocReader(xmlPath)
        shapes = tVocParseReader.getShapes()
        self.loadLabels(shapes)
        self.canvas.verified = tVocParseReader.verified

    def loadYOLOTXTByFilename(self, txtPath):
        if self.filePath is None:
            return
        if os.path.isfile(txtPath) is False:
            return

        self.set_format(FORMAT_YOLO)
        tYoloParseReader = YoloReader(txtPath, self.image)
        shapes = tYoloParseReader.getShapes()
        print(shapes)
        self.loadLabels(shapes)
        self.canvas.verified = tYoloParseReader.verified

    def togglePaintLabelsOption(self):
        paintLabelsOptionChecked = self.paintLabelsOption.isChecked()
        for shape in self.canvas.shapes:
            shape.paintLabel = paintLabelsOptionChecked

def inverted(color):
    return QColor(*[255 - v for v in color.getRgb()])


def read(filename, default=None):
    try:
        with open(filename, 'rb') as f:
            return f.read()
    except:
        return default


def get_main_app(argv=[]):
    """
    Standard boilerplate Qt application code.
    Do everything but app.exec_() -- so that we can test the application in one thread
    """
    app = QApplication(argv)
    app.setApplicationName(__appname__)
    #app.setWindowIcon(newIcon("app"))
    # Tzutalin 201705+: Accept extra agruments to change predefined class file
    # Usage : labelImg.py image predefClassFile saveDir
    win = MainWindow(argv[1] if len(argv) >= 2 else None, argv[2] if len(argv) >= 3 else "./data/predefined_classes.txt", argv[3] if len(argv) >= 4 else None)
    win.show()
    return app, win


def main():
    '''construct main app and run it'''
    app, _win = get_main_app(sys.argv)
    return app.exec_()

if __name__ == '__main__':
    sys.exit(main())
